package test.prueba;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;

/**
 * Created by Daniel on 08/01/2015.
 */

/* Esta clase contiene información estática, como el nombre de la base,
    el nombre de la tabla, los campos, la versión de la base, el query para crear la tabla.
  */
public class AsistenciasHelper extends SQLiteOpenHelper {

    public static final String TABLE_ASISTENCIAS = "asistencias";
    public static final String COLUMN_ID = "id";
    public static final String COLUMN_NAMESUBJECT = "nombreMateria";
    public static final String COLUMN_CLASE_N= "numeroClase";
    public static final String COLUMN_ASISTENCIAS = "asistio";

    private static final String DATABASE_NAME = "asistencias.db";
    private static final int DATABASE_VERSION = 1;

    // Database creation sql statement
    private static final String DATABASE_CREATE = "create table "
            + TABLE_ASISTENCIAS + "("
            + COLUMN_ID + " integer primary key autoincrement, "
            + COLUMN_NAMESUBJECT + " text not null, "
            + COLUMN_CLASE_N + " text not null, "
            + COLUMN_ASISTENCIAS + " text not null);";

    public AsistenciasHelper(Context context) {
        super(context, DATABASE_NAME, null, DATABASE_VERSION);
    }

    @Override
    public void onCreate(SQLiteDatabase database) {
        database.execSQL(DATABASE_CREATE);
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        Log.w(SubjectHelper.class.getName(),
                "Upgrading database from version " + oldVersion + " to "
                        + newVersion + ", which will destroy all old data");
        db.execSQL("DROP TABLE IF EXISTS " + TABLE_ASISTENCIAS);
        onCreate(db);
    }

}