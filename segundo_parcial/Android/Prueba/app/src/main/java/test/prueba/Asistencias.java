package test.prueba;

import android.support.v7.app.ActionBarActivity;

/**
 * Created by pcn on 25/02/2015.
 */
public class Asistencias  {
    private long id;
    private String nameSubject;
    private String claseN;
    private String asistencias;

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getNameSubject() {
        return nameSubject;
    }

    public void setNameSubject(String nameSubject) {
        this.nameSubject = nameSubject;
    }

    public String getClaseN() {
        return claseN;
    }

    public void setClaseN(String claseN) {
        this.claseN = claseN;
    }

    public String getAsistencias() {
        return asistencias;
    }

    public void setAsistencias(String asistencia) {
        this.asistencias = asistencia;
    }

    @Override
    public String toString() { return nameSubject; }
}
