package test.prueba;

import android.app.Activity;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import java.util.ArrayList;

/**
 * Created by pcn on 25/02/2015.
 */
public class ListAsistenciasAdapter extends BaseAdapter{
    protected Activity activity;
    protected ArrayList<Asistencias> asistencias;

    public ListAsistenciasAdapter(Activity activity, ArrayList<Asistencias> asistencias){
        this.activity=activity;
        this.asistencias=asistencias;
    }
    @Override
    public int getCount(){
        return asistencias.size();
    }


    public Object getItem(int position){
        return asistencias.get(position);
    }

    public long getItemId(int position){
        return asistencias.get(position).getId();
    }

    public View getView(int position, View contentView, ViewGroup parent){
        View vi = contentView;

        if(contentView==null){
            LayoutInflater inflater = (LayoutInflater)activity.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            vi = inflater.inflate(R.layout.list_main,null);
        }

        Asistencias asistencia = asistencias.get(position);


        TextView clase_n = (TextView) vi.findViewById(R.id.clase_n);
        TextView asistio=(TextView) vi.findViewById(R.id.asistencia);

        clase_n.setText(asistencia.getClaseN());
        asistio.setText(asistencia.getAsistencias());

        return vi;

    }


}
