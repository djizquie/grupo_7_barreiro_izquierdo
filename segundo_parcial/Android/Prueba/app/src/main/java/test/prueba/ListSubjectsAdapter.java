package test.prueba;

import android.app.Activity;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import java.util.ArrayList;

/**
 * Created by pcn on 25/02/2015.
 */
public class ListSubjectsAdapter extends BaseAdapter {
    protected Activity activity;
    protected ArrayList<Subject> subjects;

    public ListSubjectsAdapter(Activity activity, ArrayList<Subject> subjects){
        this.activity=activity;
        this.subjects=subjects;
    }
    @Override
    public int getCount(){
        return subjects.size();
    }


    public Object getItem(int position){
        return subjects.get(position);
    }

    public long getItemId(int position){
        return subjects.get(position).getId();
    }

    public View getView(int position, View contentView, ViewGroup parent){
        View vi = contentView;

        if(contentView==null){
            LayoutInflater inflater = (LayoutInflater)activity.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            vi = inflater.inflate(R.layout.list_main,null);
        }

        Subject subject = subjects.get(position);


        TextView nombre = (TextView) vi.findViewById(R.id.nombre_materia);

        nombre.setText(subject.getName());

        return vi;

    }


}
