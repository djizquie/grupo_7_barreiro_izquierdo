package test.prueba;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;

/**
 * Created by Daniel on 08/01/2015.
 */

/* Esta clase contiene información estática, como el nombre de la base,
    el nombre de la tabla, los campos, la versión de la base, el query para crear la tabla.
  */
public class SubjectHelper extends SQLiteOpenHelper {

    public static final String TABLE_SUBJECTS = "subjects";
    public static final String COLUMN_ID = "_id";
    public static final String COLUMN_CODE = "code";
    public static final String COLUMN_NAME = "name";
    public static final String COLUMN_HOURS = "hours";

    private static final String DATABASE_NAME = "prueba.db";
    private static final int DATABASE_VERSION = 1;

    // Database creation sql statement
    private static final String DATABASE_CREATE = "create table "
            + TABLE_SUBJECTS + "("
            + COLUMN_ID + " integer primary key autoincrement, "
            + COLUMN_CODE + " text not null, "
            + COLUMN_NAME + " text not null, "
            + COLUMN_HOURS + " text not null);";

    public SubjectHelper(Context context) {
        super(context, DATABASE_NAME, null, DATABASE_VERSION);
    }

    @Override
    public void onCreate(SQLiteDatabase database) {
        database.execSQL(DATABASE_CREATE);
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        Log.w(SubjectHelper.class.getName(),
                "Upgrading database from version " + oldVersion + " to "
                        + newVersion + ", which will destroy all old data");
        db.execSQL("DROP TABLE IF EXISTS " + TABLE_SUBJECTS);
        onCreate(db);
    }

}