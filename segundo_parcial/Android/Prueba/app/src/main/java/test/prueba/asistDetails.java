package test.prueba;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.ActionBarActivity;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.TextView;

import java.util.ArrayList;

import butterknife.ButterKnife;
import butterknife.InjectView;

/**
 * Created by pcn on 25/02/2015.
 */
public class asistDetails extends ActionBarActivity {

    @InjectView(R.id.nombre_materia) TextView nameSubject;
    @InjectView(R.id.hours) TextView hours;
    private SubjectDataSource dataSource;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.details);
        ButterKnife.inject(this);
        dataSource = new SubjectDataSource(this);
        dataSource.open();
        ArrayList<Subject> subjects = dataSource.getAllSubjects();
        //for(Subject s: subjects){
        // System.out.println(s.getCode());
        //System.out.println(s.getName());
        //System.out.println(s.getHours());
        String str=getIntent().getStringExtra("nombre");
        Subject sbj=null;
        for (Subject s:subjects){
            if (str.equals(s.getName())){
                sbj=s;
            }
        }

        nameSubject.setText(sbj.getName());
        hours.setText(sbj.getHours());

    }

    @Override
    public void onDestroy(){
        dataSource.close();
        super.onDestroy();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.subject_options, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch( item.getItemId()){
            case R.id.asistencias:{
                Intent registerSubject = new Intent(getApplicationContext(), RegisterSubject.class);
                startActivity(registerSubject);
                return true;}
            case R.id.proyectos:{
                Intent registerSubject = new Intent(getApplicationContext(), RegisterSubject.class);
                startActivity(registerSubject);
                return true;}
            case R.id.examenes:{
                Intent registerSubject = new Intent(getApplicationContext(), RegisterSubject.class);
                startActivity(registerSubject);
                return true;}
            case R.id.tareas:{
                Intent registerSubject = new Intent(getApplicationContext(), RegisterSubject.class);
                startActivity(registerSubject);
                return true;}

        }

        return super.onOptionsItemSelected(item);
    }
}
