package test.prueba;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.SQLException;
import android.database.sqlite.SQLiteDatabase;

import java.util.ArrayList;

/**
 * Created by Daniel on 08/01/2015.
 */

/*Esta Clase posee los métodos necesarios para la manipulación de la base:
* Abrir conexión
* Cerrar conexión
* Crear un usuario
* Borrar el usuario
* Actualizar el usuario
* Buscar un usuario
* Validar un usuario para el inicio de sesión
* Encontrar todos los usuarios
*/
public class SubjectDataSource {
    // Database fields
    private SQLiteDatabase database;
    private SubjectHelper dbHelper;
    private String[] allColumns = {
            SubjectHelper.COLUMN_ID,
            SubjectHelper.COLUMN_CODE,
            SubjectHelper.COLUMN_NAME,
            SubjectHelper.COLUMN_HOURS };

    public SubjectDataSource(Context context) {
        dbHelper = new SubjectHelper(context);
    }

    public void open() throws SQLException {
        database = dbHelper.getWritableDatabase();
    }

    public void close() {
        dbHelper.close();
    }

    public Subject createSubject(String code, String name, String hours) {
        ContentValues values = new ContentValues();
        values.put(SubjectHelper.COLUMN_CODE, code);
        values.put(SubjectHelper.COLUMN_NAME, name);
        values.put(SubjectHelper.COLUMN_HOURS, hours);
        long insertId = database.insert(SubjectHelper.TABLE_SUBJECTS, null, values);
        Cursor cursor = database.query(SubjectHelper.TABLE_SUBJECTS,
                allColumns, SubjectHelper.COLUMN_ID + " = " + insertId, null,
                null, null, null);
        cursor.moveToFirst();
        Subject subject = cursorToSubject(cursor);
        cursor.close();
        return subject;
    }

    public void deleteSubject(Subject subject) {
        long id = subject.getId();
        database.delete(SubjectHelper.TABLE_SUBJECTS, SubjectHelper.COLUMN_ID + " = " + id, null);
        System.out.println("User deleted with id: " + id);
    }

    public void updateSubject(Subject subject) {
        long id = subject.getId();
        ContentValues values = new ContentValues();
        values.put(SubjectHelper.COLUMN_CODE, subject.getCode());
        values.put(SubjectHelper.COLUMN_NAME, subject.getName());
        values.put(SubjectHelper.COLUMN_HOURS, subject.getHours());
        database.update(SubjectHelper.TABLE_SUBJECTS,values , SubjectHelper.COLUMN_ID+ " = " + id, null);
        System.out.println("User updated with id: " + id);
    }

    public Subject findSubject(long _id){
        Cursor cursor = database.query(SubjectHelper.TABLE_SUBJECTS,allColumns, null, null, null, null, null);
        cursor.moveToFirst();
        while (!cursor.isAfterLast()) {
            Subject subject = cursorToSubject(cursor);
            if(subject.getId() == _id)
                return subject;
            cursor.moveToNext();
        }
        return null;
    }

    public ArrayList<Subject> getAllSubjects() {
        ArrayList<Subject> users = new ArrayList<Subject>();
        Cursor cursor = database.query(SubjectHelper.TABLE_SUBJECTS,
                allColumns, null, null, null, null, null);
        cursor.moveToFirst();
        while (!cursor.isAfterLast()) {
            Subject subject = cursorToSubject(cursor);
            users.add(subject);
            cursor.moveToNext();
        }
        // make sure to close the cursor
        cursor.close();
        return users;
    }

    private Subject cursorToSubject(Cursor cursor) {
        Subject subject = new Subject();
        subject.setId(cursor.getLong(0));
        subject.setCode(cursor.getString(1));
        subject.setName(cursor.getString(2));
        subject.setHours(cursor.getString(3));
        return subject;
    }
}
