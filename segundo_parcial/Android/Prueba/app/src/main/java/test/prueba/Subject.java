package test.prueba;

/**
 * Created by Denny on 08/01/2015.
 */

public class Subject {

    private long id;
    private String code;
    private String name;
    private String hours;

    public String getCode() {
        return code;
    }

    public String getHours() {
        return hours;
    }

    public void setHours(String hours) {
        this.hours = hours;
    }

    public void setCode(String code) {
        this.code = code;

    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public long getId() {

        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    @Override
    public String toString() { return name; }
}
