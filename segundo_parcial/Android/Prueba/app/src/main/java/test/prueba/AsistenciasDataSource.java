package test.prueba;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.SQLException;
import android.database.sqlite.SQLiteDatabase;

import java.util.ArrayList;

/**
 * Created by Daniel on 08/01/2015.
 */

/*Esta Clase posee los métodos necesarios para la manipulación de la base:
* Abrir conexión
* Cerrar conexión
* Crear un usuario
* Borrar el usuario
* Actualizar el usuario
* Buscar un usuario
* Validar un usuario para el inicio de sesión
* Encontrar todos los usuarios
*/
public class AsistenciasDataSource {
    // Database fields
    private SQLiteDatabase database;
    private SubjectHelper dbHelper;
    private String[] allColumns = {

            AsistenciasHelper.COLUMN_ID,
            AsistenciasHelper.COLUMN_NAMESUBJECT,
            AsistenciasHelper.COLUMN_CLASE_N,
            AsistenciasHelper.COLUMN_ASISTENCIAS,
             };

    public AsistenciasDataSource(Context context) {
        dbHelper = new SubjectHelper(context);
    }

    public void open() throws SQLException {
        database = dbHelper.getWritableDatabase();
    }

    public void close() {
        dbHelper.close();
    }

    public Asistencias createAsistencia(String nombreSubj,String n_clase, String asistencias) {
        ContentValues values = new ContentValues();
        values.put(AsistenciasHelper.COLUMN_NAMESUBJECT, nombreSubj);
        values.put(AsistenciasHelper.COLUMN_CLASE_N, n_clase);
        values.put(AsistenciasHelper.COLUMN_ASISTENCIAS, asistencias);
        long insertId = database.insert(AsistenciasHelper.TABLE_ASISTENCIAS, null, values);
        Cursor cursor = database.query(AsistenciasHelper.TABLE_ASISTENCIAS,
                allColumns, SubjectHelper.COLUMN_ID + " = " + insertId, null,
                null, null, null);
        cursor.moveToFirst();
        Asistencias asistencia = cursorToAsistencia(cursor);
        cursor.close();
        return asistencia;
    }

    public void deleteAsistencia(Asistencias asistencia) {
        long id = asistencia.getId();
        database.delete(AsistenciasHelper.TABLE_ASISTENCIAS, AsistenciasHelper.COLUMN_ID + " = " + id, null);
        System.out.println("User deleted with id: " + id);
    }

    public void updateAsitencia(Asistencias asistencia) {
        long id = asistencia.getId();
        ContentValues values = new ContentValues();
        values.put(AsistenciasHelper.COLUMN_NAMESUBJECT, asistencia.getNameSubject());
        values.put(AsistenciasHelper.COLUMN_CLASE_N, asistencia.getClaseN());
        values.put(AsistenciasHelper.COLUMN_ASISTENCIAS, asistencia.getAsistencias());
        database.update(AsistenciasHelper.TABLE_ASISTENCIAS,values , AsistenciasHelper.COLUMN_ID+ " = " + id, null);
        System.out.println("User updated with id: " + id);
    }

    public Asistencias findAsistencias(String nombre){
        Cursor cursor = database.query(AsistenciasHelper.TABLE_ASISTENCIAS,allColumns, null, null, null, null, null);
        cursor.moveToFirst();
        while (!cursor.isAfterLast()) {
            Asistencias asistencia = cursorToAsistencia(cursor);
            if(asistencia.getNameSubject().equals(nombre))
                return asistencia;
            cursor.moveToNext();
        }
        return null;
    }

    public ArrayList<Asistencias> getAllAsistencias() {
        ArrayList<Asistencias> users = new ArrayList<Asistencias>();
        Cursor cursor = database.query(AsistenciasHelper.TABLE_ASISTENCIAS,
                allColumns, null, null, null, null, null);
        cursor.moveToFirst();
        while (!cursor.isAfterLast()) {
            Asistencias asistencia = cursorToAsistencia(cursor);
            users.add(asistencia);
            cursor.moveToNext();
        }
        // make sure to close the cursor
        cursor.close();
        return users;
    }

    private Asistencias cursorToAsistencia(Cursor cursor) {
        Asistencias asistencia = new Asistencias();
        asistencia.setId(cursor.getLong(0));
        asistencia.setNameSubject(cursor.getString(1));
        asistencia.setClaseN(cursor.getString(2));
        asistencia.setAsistencias(cursor.getString(3));
        return asistencia;
    }
}
