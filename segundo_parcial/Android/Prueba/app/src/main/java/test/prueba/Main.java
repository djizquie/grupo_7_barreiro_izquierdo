package test.prueba;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.ActionBarActivity;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;

import java.util.ArrayList;

import butterknife.ButterKnife;
import butterknife.InjectView;


public class Main extends ActionBarActivity {

    //@InjectView(R.id.main_subjects) TextView textview;
    private SubjectDataSource dataSource;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.main);
        ButterKnife.inject(this);
        dataSource = new SubjectDataSource(this);
        dataSource.open();
        ArrayList<Subject> subjects = dataSource.getAllSubjects();
        //for(Subject s: subjects){
           // System.out.println(s.getCode());
            //System.out.println(s.getName());
            //System.out.println(s.getHours());

            ListView lv = (ListView)findViewById(R.id.listView);

            ListSubjectsAdapter adapter = new ListSubjectsAdapter(this, subjects);

            lv.setAdapter(adapter);
        lv.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                LinearLayout ll=(LinearLayout)view;
                TextView t=(TextView)ll.findViewById(R.id.nombre_materia);
                    Intent infoMateria = new Intent(getApplicationContext(), Details.class);
                infoMateria.putExtra("nombre",t.getText());
                startActivity(infoMateria);
            }
        });
            //if(textview.getText().length()>0)
              //  textview.setText(textview.getText() + ", " + s.getName());
            //else
              //  textview.setText(s.getName());
        //}
    }

    @Override
    public void onDestroy(){
        dataSource.close();
        super.onDestroy();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        if (id == R.id.action_add_subject) {
            Intent registerSubject = new Intent(getApplicationContext(), RegisterSubject.class);
            startActivity(registerSubject);
            return true;
        }

        return super.onOptionsItemSelected(item);
    }
}
