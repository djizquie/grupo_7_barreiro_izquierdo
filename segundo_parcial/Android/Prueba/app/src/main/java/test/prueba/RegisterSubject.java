package test.prueba;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.ActionBarActivity;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.EditText;

import butterknife.ButterKnife;
import butterknife.InjectView;
import butterknife.OnClick;

/**
 * Created by Daniel on 24/02/2015.
 */
public class RegisterSubject extends ActionBarActivity {

    @InjectView(R.id.register_subject_code) EditText code;
    @InjectView(R.id.register_subject_name) EditText name;
    @InjectView(R.id.register_subject_hours) EditText hours;

    private SubjectDataSource dataSource;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.registersubject);
        ButterKnife.inject(this);
        dataSource = new SubjectDataSource(this);
        dataSource.open();
    }

    @OnClick(R.id.register_subject_button)
    public void click(){
        String codigo = code.getText().toString();
        String nombre = name.getText().toString();
        String horas = hours.getText().toString();
        dataSource.createSubject(codigo,nombre,horas);
        finish();
        Intent main = new Intent(getApplicationContext(), Main.class);
        startActivity(main);
    }

    @Override
     public void onDestroy(){
        dataSource.close();
        super.onDestroy();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        if (id == R.id.action_add_subject) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

}
