;;Author: Daniel Izquierdo & Jhon Barreiro
;;****
;;****THE FOLLOWING LINES ARE AN IMPLEMENTATION OF THE CODE SHOWED IN:
;;****http://gigamonkeys.com/book/practical-a-simple-database.html

;;definition of the variable that will contain the data base
(defvar *dbSubjects* nil)
(defvar *dbTareas* nil)
(defvar *dbAsistencias* nil)
(defvar *dbExamenes* nil)
(defvar *dbProyectos* nil)

(setq num_actual_clases 0)
(setq num_actual_proyectos 0)
(setq num_actual_examenes 0)
(setq num_actual_tareas 0)

(setq sumClasesAsistidas 0)
(setq sumTareasEntregadas 0)
(setq sumNotasExamenes 0)
(setq sumNotasProyectos 0)

(defvar *op* nil)
(setq *listSubjects* (list ))
(defparameter *hashSubjects* (make-hash-table))

(defun mostrarListaMaterias ()
  (maphash #'print-hash-entry *hashSubjects*)
  (terpri)
)

(defun llenarListaMaterias ()

  (setq count 1)
  (select (where :materia nil) *dbSubjects*)

  (loop for x in *listSubjects*
    do ((lambda ()
      (if (not (equal x nil))
        (progn (setf (gethash count *hashSubjects*) x)
        (setq count (+ count 1)))
      )
      )
    )
            
  )

)

(defun print-hash-entry (key value)
    (format t "[~S] ~a~%" key value))

;;definition of the function that will allow to CREATE a new Subject of the student
(defun make-subject (id materia n_hours)
  (setq par2 0)
  (princ "length = ")(princ (length *listSubjects*))
  (loop for x in *listSubjects* do
    (write-line x)
    (if (equal x materia)
      (progn (terpri)(write-line "----->  Alerta!! :  Materia existente")(terpri))
      (setq par2 (+ par2 1))
    )
  )

  (if (equal par2 (length *listSubjects*))
    (progn (write-line "-----> Ingreso exitoso.")
      (list :id id :materia materia :n_hours n_hours)))
  (list :id id :materia materia :n_hours n_hours))

(defun make-tarea (materia tarea entrego)
  (setq materiaSelected (gethash (parse-integer materia) *hashSubjects*))
  (list :materia materiaSelected :tarea tarea :entrego entrego))

(defun make-asistencia (materia clase asistio)
  (setq materiaSelected (gethash (parse-integer materia) *hashSubjects*))
  (list :materia materiaSelected :clase clase :asistio asistio))

(defun make-proyecto (materia proyecto nota)
  (setq materiaSelected (gethash (parse-integer materia) *hashSubjects*))
  (list :materia materiaSelected :proyecto proyecto :nota nota))

(defun make-examen (materia examen nota)
  (setq materiaSelected (gethash (parse-integer materia) *hashSubjects*))
  (list :materia materiaSelected :examen examen :nota nota))

(defun resetearValores ()
  (setq num_actual_clases 0)
  (setq num_actual_tareas 0)
  (setq num_actual_proyectos 0)
  (setq num_actual_examenes 0)
  (setq sumClasesAsistidas 0)
  (setq sumTareasEntregadas 0)
  (setq sumNotasExamenes 0)
  (setq sumNotasProyectos 0)
  )

(defun PorcentajeAsistencia ()

  (terpri)
  (header)
  (mostrarListaMaterias)
  (princ "Escoja Materia: ")
  (setq materiaSelected (gethash (read) *hashSubjects*))
  (select (where :materia materiaSelected) *dbAsistencias*)

  (princ "sumClasesAsistidas: ")(princ sumClasesAsistidas)(terpri)
  (princ "num_actual_clases: ")(princ num_actual_clases)(terpri)

  (princ "Porcentaje de Asistencias: ")
  (setq porcentajeAsistencia (float (* (/ sumClasesAsistidas num_actual_clases) 100))) (princ "%")
  (terpri)
  (princ porcentajeAsistencia)

  (setq num_actual_clases 0)
  (setq sumClasesAsistidas 0)
  )

(defun RendimientoTareas ()

  (terpri)
  (header)
  (mostrarListaMaterias)
  (princ "Escoja Materia: ")
  (setq materiaSelected (gethash (read) *hashSubjects*))
  (select (where :materia materiaSelected) *dbTareas*)

  (princ "Rendimiento de Tareas: ")
  (setq rendimientoTareas (float (* (/ sumTareasEntregadas num_actual_tareas) 100))) (princ "%")
  (terpri)
  (princ rendimientoTareas)

  (setq num_actual_tareas 0)
  (setq sumTareasEntregadas 0)
  )

(defun RendimientoExamenes ()

  (terpri)
  (header)
  (mostrarListaMaterias)
  (princ "Escoja Materia: ")
  (setq materiaSelected (gethash (read) *hashSubjects*))
  (select (where :materia materiaSelected) *dbExamenes*)

  (princ "Rendimiento de Examenes: ")
  (setq rendimientoExamenes (float (* (/ sumNotasExamenes (* num_actual_examenes 100)) 100))) (princ "%")
  (princ rendimientoExamenes)

  (setq num_actual_examenes 0)
  (setq sumNotasExamenes 0)
  )

(defun RendimientoProyectos (&optional sumNotasProyectos numProyectos)

  (terpri)
  (header)
  (mostrarListaMaterias)
  (princ "Escoja Materia: ")
  (setq materiaSelected (gethash (read) *hashSubjects*))
  (select (where :materia materiaSelected) *dbProyectos*)

  (princ sumNotasProyectos)
  (terpri)
  (princ num_actual_proyectos)
  (terpri)

  (princ "Rendimiento de Proyectos: ")
  (setq rendimientoProyectos (float (* (/ sumNotasProyectos (* num_actual_proyectos 100)) 100))) (princ "%")
  (princ rendimientoProyectos)

  (setq num_actual_proyectos 0)
  (setq sumNotasProyectos 0)
  )

(defun RendimientoGlobalxMateria ()

  (terpri)
  (header)
  (mostrarListaMaterias)
  (princ "Escoja Materia: ")
  (setq materiaSelected (gethash (read) *hashSubjects*))

  (select (where :materia materiaSelected) *dbExamenes*)
  (select (where :materia materiaSelected) *dbTareas*)
  ;(select (where :materia materiaSelected) *dbExamenes*)

  
  (princ sumNotasExamenes)(terpri)
  (princ sumTareasEntregadas)(terpri)
  (princ num_actual_examenes)(terpri)
  

  (setq rendimientoGlobal (float (* 100 (/ (+ (* sumNotasExamenes 0.9) (* sumTareasEntregadas 0.1)) (* num_actual_examenes 100)))))
  (princ "Rendimiento Global: ")
  (princ rendimientoGlobal)(princ "%")
  (terpri)
  (terpri)

  (setq sumNotasExamenes 0)
  (setq num_actual_examenes 0)
  (setq sumTareasEntregadas 0)
  )

;;prints with a better format
(defun dump-db (db par)

	(setq filter nil)

  	(if (or (equal par 1)
  			(equal par 2)
  			(equal par 3)
  			(equal par 4))
  		(progn
      (header)
  		(mostrarListaMaterias)
      (princ "Escoja materia: ")
      (setq filter (gethash (read) *hashSubjects*))
  		(format t "~{~{~a:~10t~a~%~}~%~}" (select (where :materia filter) db)) 
  		(terpri)
  		(case par 
  			(1  (princ "Numero de clases actuales: ")
  				  (princ num_actual_clases)
  				  (terpri)
            (princ "Total clases asistidas: "))

  			(2  (princ "Numero de tareas actuales: ")
  				  (princ num_actual_tareas)
  			   	(terpri)
            (princ "Total tareas entregadas: "))

  			(3  (princ "Numero de proyectos actuales: ")
  	   			(princ num_actual_proyectos)
  	   			(terpri)
            (princ sumNotasProyectos))

  			(4  (princ "Numero de examenes actuales: ")
  		  		(princ num_actual_examenes)
            (terpri))
  			)
  		
  		(terpri)
  		(prompt-read "Presione enter para continuar...")
      (resetearValores)
  		(princ (system "clear"))
  		;(setq db (select (where :materia filter) db))	
  		)
  		(progn
			(terpri)
			(terpri)
  			(format t "~{~{~a:~10t~a~%~}~%~}" db)
  			;(format t "~{~{~a:~10t~a~%~}~%~}" db)
  			(terpri)
  			(prompt-read "Presione enter para continuar::")
  			(princ (system "clear")))
  		)

 )


;;prompt the user for information about a set of subjects
;;The call to FORCE-OUTPUT is necessary in some implementations to ensure that Lisp doesn't wait for a newline before it prints the prompt.
(defun prompt-read (prompt)
  (format *query-io* "~a: " prompt)
  (force-output *query-io*)
  (read-line *query-io*))


;;to read from a prompt
(defun prompt-for-sbj ()
  (header)
  (make-subject
   (prompt-read "id de la materia")
   (prompt-read "nombre de la materia")
   (or (parse-integer (prompt-read "horas") :junk-allowed t) 0)
  ))

(defun prompt-for-asistencia ()
  (header)
  (mostrarListaMaterias)
  (make-asistencia
   (prompt-read "Escoja Materia")
   (prompt-read "clase")
   (or (parse-integer (prompt-read "asistio") :junk-allowed t) 0)
  ))

(defun prompt-for-tarea ()
  (header)
  (mostrarListaMaterias)
  (make-tarea
   (prompt-read "Escoja Materia")
   (prompt-read "tarea")
   (or (parse-integer (prompt-read "cumplio") :junk-allowed t) 0)
  ))

(defun prompt-for-examen()
  (header)
  (mostrarListaMaterias)
  (make-examen
   (prompt-read "Escoja Materia")
   (prompt-read "examen")
   (or (parse-integer (prompt-read "nota") :junk-allowed t) 0)
  ))

(defun prompt-for-proyecto ()
  (header)
  (mostrarListaMaterias)
  (make-proyecto
   (prompt-read "Escoja Materia")
   (prompt-read "proyecto")
   (or (parse-integer (prompt-read "nota") :junk-allowed t) 0)
  ))

(defun add-subjects (&optional db)
  (loop (push (prompt-for-sbj) *dbSubjects*)
      (if (not (y-or-n-p "Another? [y/n]: ")) (return))))

(defun add-asistencias (&optional db)
  (loop (push (prompt-for-asistencia) *dbAsistencias*)
      (if (not (y-or-n-p "Another? [y/n]: ")) (return))))

(defun add-tareas (&optional db)
  (loop (push (prompt-for-tarea) *dbTareas*)
      (if (not (y-or-n-p "Another? [y/n]: ")) (return))))

(defun add-examenes (&optional db)
  (loop (push (prompt-for-examen) *dbExamenes*)
      (if (not (y-or-n-p "Another? [y/n]: ")) (return))))

(defun add-proyectos (&optional db)
  (loop (push (prompt-for-proyecto) *dbProyectos*)
      (if (not (y-or-n-p "Another? [y/n]: ")) (return))))


;;SAVE the database in a file
(defun save-db (filename)

  (with-open-file (out filename
                   :direction :output
                   :if-exists :supersede)
  (if (equal filename "my-Subjects.db")
  		  		(with-standard-io-syntax
      		(print *dbSubjects* out)))

  	
  	(if (equal filename "my-Asistencias.db")
  		  		(with-standard-io-syntax
      		(print *dbAsistencias* out))

  	)
  	(if (equal filename "my-Tareas.db")
  		  		(with-standard-io-syntax
      		(print *dbTareas* out))

  	)
  	(if (equal filename "my-Examenes.db")
  		  		(with-standard-io-syntax
      		(print *dbExamenes* out))

  	)
  	(if (equal filename "my-Proyectos.db")
  		  		(with-standard-io-syntax
      		(print *dbProyectos* out))

  	))
)
    
      

;;LOAD the database in the global variable dbSubjects
(defun load-db (filename)
  (with-open-file (in filename :if-does-not-exist :create)

  	(if (equal filename "my-Subjects.db")
  		  		(with-standard-io-syntax
      		(setf *dbSubjects* (read in)))

  	)
  	(if (equal filename "my-Asistencias.db")
  		  		(with-standard-io-syntax
      		(setf *dbAsistencias* (read in)))

  	)
  	(if (equal filename "my-Tareas.db")
  		  		(with-standard-io-syntax
      		(setf *dbTareas* (read in)))

  	)
  	(if (equal filename "my-Examenes.db")
  		  		(with-standard-io-syntax
      		(setf *dbExamenes* (read in)))

  	)
  	(if (equal filename "my-Proyectos.db")
  		  		(with-standard-io-syntax
      		(setf *dbProyectos* (read in)))

  	)
  )
)

;;selector for a query according to selector-fn function
(defun select (selector-fn db)
  (remove-if-not selector-fn db)
  ; (remove-if-not selector-fn *dbProyectos*)
  ; (remove-if-not selector-fn *dbExamenes*)
  ; (remove-if-not selector-fn *dbTareas*)
  ; (remove-if-not selector-fn *dbAsistencias*)
  )

;;to use in the selector to looking for a subject by id or name according to parameters inputed by the user
(defun where (&key id materia asistio entrego proyecto examen)
  (if (equal materia nil)
  (progn (setq asistio 0)
  (setq entrego 0)
  (setq proyecto 0)
  (setq examen 0)))

  #'(lambda (subject)
      (and
       (if id (equal (getf subject :id)  id) (setq *listSubjects* (append *listSubjects* (list (getf subject :materia)))))
       (if materia (equal (getf subject :materia) materia) t)
       (if asistio (equal (getf subject :asistio) asistio) (if (and (not (equal (getf subject :asistio) nil))
                                                                    (equal (getf subject :asistio) 1))
                                                                    (progn (setq sumClasesAsistidas (+ sumClasesAsistidas 1))
                                                                    (setq num_actual_clases (+ num_actual_clases 1)))
                                                                    (setq num_actual_clases (+ num_actual_clases 1))))
       (if entrego (equal (getf subject :entrego) entrego) (if (and (not (equal (getf subject :entrego) nil))
                                                                    (equal (getf subject :entrego) 1))
                                                                    (progn (setq sumTareasEntregadas (+ sumTareasEntregadas 1))
                                                                           (setq num_actual_tareas (+ num_actual_tareas 1)))
                                                                    (setq num_actual_tareas (+ num_actual_tareas 1))))
       (if proyecto (equal (getf subject :proyecto) proyecto) (if (not (equal (getf subject :proyecto) nil))
                                                                  (progn 
                                                                  (setq num_actual_proyectos (+ num_actual_proyectos 1))
                                                                  (setq sumNotasProyectos (+ sumNotasProyectos (getf subject :nota))))
                                                                  t))
       (if examen (equal (getf subject :examen) examen) (if (not (equal (getf subject :examen) nil))
                                                                  (progn 
                                                                  (setq num_actual_examenes (+ num_actual_examenes 1))
                                                                  (setq sumNotasExamenes (+ sumNotasExamenes (getf subject :nota))))
                                                                  t))
       )))

;;used to update the database
(defun update (selector-fn &key (id nil id-p) materia n_hours)
  (setf *dbSubjects*
        (mapcar
         #'(lambda (row)
             (when (funcall selector-fn row)
               (if materia    (setf (getf row :materia) materia))
               (if id   (setf (getf row :id) id))
               (if n_hours   (setf (getf row :n_hours) n_hours))
              )
             row) *dbSubjects*)))


;edit a subject by id or name
(defun edit-subject()
	(loop
			(loop 
				    (header)
					 (print "ingrese nombre materia: ")
						(setq materia (read-line))
						(princ (select (where :materia materia) *dbSubjects*))
						(terpri)
						(write-line "¿Que desea modificar? ")
						(write-line "[1] id")
						(write-line "[2] Nombre")
						(write-line "[3] Horas")
						(setq op2 (read))
						(case op2
							(1 (print "Ingrese nuevo id")
								(setq idn (read-line))
								(update (where :materia materia) :id idn)            
								(return (princ (select (where :materia materia)  *dbSubjects*))));final case 1 interior
							(2(print "Ingrese nuevo nombre")
								(setq materian(read-line))
								(update (where :materia materia) :materia materian)            
								(return (princ (select (where :materia materia)  *dbSubjects*))));final case 2 interior
							(3(print "Ingrese nuevo n_horas")
								(setq horasn(read-line))
								(update (where :materia materia) :n_hours horasn)            
								(return (princ (select (where :materia materia)  *dbSubjects*))));final case 1 interior)
						)
					
					
					);;cierre del loop
(terpri)

(return (prompt-read "presione enter para continuar"))
);;cierre del loop principal
);;cierre de funcion

;copy the inputed stream to use with the system
(defun copy-stream (in out)
   (loop for line = (read-line in nil nil)
         while line
         do (write-line line out)))

;controls the system, in this case linux
(defun system (cmd)
  (with-open-stream (s1 (ext:run-program cmd :output :stream))
    (with-output-to-string (out)
      (copy-stream s1 out))))

(defun guardar ()
	(save-db "my-Subjects.db")
 	(save-db "my-Asistencias.db")
  	(save-db "my-Tareas.db")
  	(save-db "my-Examenes.db")
  	(save-db "my-Proyectos.db")
	)



;;definition of the main menu
(defun menu()
  (llenarListaMaterias)
	(loop do
		(loop do
			(header)
			(write-line "¿Qué desea hacer?")
			(terpri)
			(write-line "[1] Consultar")
			(write-line "[2] Ingresar")
			(write-line "[3] Editar")
			(write-line "[4] Guardar")
			(write-line "[5] Exit")
			(terpri)
			(write-line "***Recuerde guardar para actualizar los cambios***")
			(terpri)
			(princ "Escoja opción: ")
			(setq op (read))
			while(or (> op 5)  (< op 1)))
		(if (/= op 5)
			(loop
				(case op
					(1 (return(menuInformation)))
					(2 (return(menuInformation)))
          			(3 (return(menuInformation)))
                (4 (return(guardar)))
          			);;cierre del case
          			);;cierre del loop
          			);;cierre del if
		while(/= op 5)
		);;cierre del loop do principal
		);;cierre de funcion

(defun menuRendimientos()
  (loop do
    (loop do
      (header)
      (write-line "¿Qué desea Consultar?")
      (terpri)
      (write-line "[1] Porcentaje de Asistencia")
      (write-line "[2] Rendimiento de Examenes")
      (write-line "[3] Rendimiento de Tareas")
      (write-line "[4] Rendimiento de Proyectos")
      (write-line "[5] Rendimiento Global por Materia")
      (write-line "[6] Regresar")
      (terpri)
      (princ "Escoja opción: ")
      (setq op (read))
      while(or (> op 6)  (< op 1)))
    (if (/= op 6)
      (loop
        (case op
          (1 (return(PorcentajeAsistencia)))
          (2 (return(RendimientoExamenes)))
                (3 (return(RendimientoTareas)))
                (4 (return(RendimientoProyectos)))
                (5 (return(RendimientoGlobalxMateria)))
                );;cierre del case
                );;cierre del loop
                );;cierre del if
    while(/= op 6)
    );;cierre del loop do principal
    );;cierre de funcion

;first secondary menu (choosing option 1 in the main menu)
(defun menuInformation()
  
  (loop do
  	(loop do
  		;(princ (system "clear"))
      (header)
  		(if (= op 1)
  			(write-line "¿Qué desea Consultar?"))
  		(if (= op 2)
  			(write-line "¿Qué desea Ingresar?"))
  		(if (= op 3)
  			(write-line "¿Qué desea Editar?"))
  		(if (= op 4)
  			(write-line "¿Qué desea Eliminar?"))
  		(terpri)
    	(write-line "   [1] Materias")
    	(write-line "   [2] Asistencias")
    	(write-line "   [3] Tareas")
      	(write-line "   [4] Proyectos")
      	(write-line "   [5] Exámenes")
      	(write-line "   [6] Rendimientos")
        (write-line "   [7] Regresar")
      	(terpri)
      	(princ "    Escoja opción: ")
      	(setq op1 (read))
      	(terpri)
      	while(or (> op1 7)  (< op1 1)))
  	(if (/= op1 7)
  		(loop
  			(case op1
  				;(1 (return(princ "INGRESAR MATERIAS"))
  				(1 (return (if (= op 1) (dump-db *dbSubjects* 0)
  						   (if (= op 2) (add-subjects *dbSubjects*)
  						   (if (= op 3) (edit-subject)
  						   (if (= op 4) (princ "ELIMINAR MATERIA")))))))

  				(2 (return (if (= op 1) (dump-db *dbAsistencias* 1)
  					       (if (= op 2) (add-asistencias)
  						   (if (= op 3) (princ "EDITAR ASISTENCIAS")
  						   (if (= op 4) (princ "ELIMINAR ASISTENCIAS")))))))

  				(3 (return (if (= op 1) (dump-db *dbTareas* 2)
  					       (if (= op 2) (add-tareas)
  						   (if (= op 3) (princ "EDITAR TAREAS")
  						   (if (= op 4) (princ "ELIMINAR TAREAS")))))))

  				(4 (return (if (= op 1) (dump-db *dbProyectos* 3)
  					       (if (= op 2) (add-proyectos)
  						   (if (= op 3) (princ "EDITAR PROYECTOS")
  						   (if (= op 4) (princ "ELIMINAR PROYECTOS")))))))

  				(5 (return (if (= op 1) (dump-db *dbExamenes* 4)
  					       (if (= op 2) (add-examenes)
  						   (if (= op 3) (princ "EDITAR EXAMENES")
  						   (if (= op 4) (princ "ELIMINAR EXAMENES")))))))

          (6 (menuRendimientos))
  				);;cierre del case
  				);;cierre del loop
  				);;cierre del if
  	(terpri)
  	while(/= op1 7)
  	);;cierre del loop do principal
);;cierre de funcion

;choosing option 1.1
(defun menuSubj()
  
  (loop do
    (loop do
      (header)
      (terpri)
      (write-line "    ***** MATERIAS *****")
      (terpri)
      (write-line "     [1] info general")
      (write-line "     [2] Ingresar")
      (write-line "     [3] Editar")
      (write-line "     [4] Eliminar")
      (write-line "     [5] Regresar")
      (terpri)
      (princ "      Escoja opción: ")
      (setq op (read))
      (terpri)
      while(or (> op 5)  (< op 1)))
     (if (/= op 5)
      (loop 
  (case op
    (1 (print " Ingrese Materia: ");;first, input the name of the 
    (setq materia (read-line))
    (return (princ (select (where :materia materia) *dbSubjects*))))
    (2 (return(add-subjects)))
    (3 (return(edit-subject)))
   );;cierre del case
      );;cierre del loop
     );;cierre del if
     (terpri)
      while(/= op 5)
    );;cierre del loop do principal
);;cierre de funcion


(defun header()
(princ (system "clear"))
(print"       ,@@.   i#is;  sr  :@#i;riissiiiiiiiiissiri&9:     ;AGrsisSX5r;;rir.  :X2i##:   &M,        ;@@         ")
(print"       X@r    ,iXAX.    ;@Brsisiissiisssiissrriss2r  ;i,  iXrriSs,.. .:  :is .3ir#@:  :    :S3,   5@X        ")
(print"      ;@@    si  :iBr  :@@;rsssssiissiiiiiiiiiSSSisr5B#Ai;s5i5S;  rs;:,  ,,Xr :X2sH@:   rXAH3Xr    @@:       ")
(print"      B@:    .  :,.   :@@;rsssssssiisssssssrrissSS53XSS2X52Ss2r :AS  :.  i;;;  ,iiS@#. ,BB   5X.   ;@H       ")
(print"     ;@A   ,AHr: .3,  H@s;sissssssisrrsssii52XS5225X5rsS2isiii, 2B3:.i#S ;,, :s. S5i@H  .rrs22,     #@:      ")
(print"     A@:    ,:;riX;  r@h:risssiissiiiisiS22Ss;,.,,;sXXisissi5S, s922339hi,..rG#G .irh@r   ri;,5H;   r@A      ")
(print"    ,@@   .i: :Xs   .HArrssiiisssiisSsiXXr,..,,;;:..,;ii23iri3i .SXiiS2S;,riisr: ;Sir#M  .hSsH5ri.   @@,     ")
(print"    r@9   ,X&BBSS5  ;BXrisssiissssssiXi.   .r22XXXS,    .:s2SShr  2XriSiS;::,,:  52i;h#;  s. :  .    X@r     ")
(print"    2@r    .r,  ;;  5MS;iissssiiisi22. ...,r;,.,,::;s3r  ,..i922s  ;25SsS3GGi:,,r2Ss;S@X  :;..,;r,   r@2     ")
(print"    H@:            .AMr;SisiisssSS5r .shB9,.23:,,.;hH5.;AHi. ,23X3;  sh5i2r .:r33iiisrBA  ,BXS5X3;   :@H     ")
(print"    #@,      ;Shh,  AArriisiSirsSi:  sG22M2..,,r2s, :s.:5339i. XXSAi  r9Xr .2A9XisssisAH. ,s.        .@#     ")
(print"    @@,   r#@#9:    A&rsiiisiisiSr  :..;Ss,,i,;X393r. r, ,rsr.  rXhh;.rS5; ;32r;rssrsi&H,  ,,        .@@     ")
(print"    @@.    . .;r:   HArsSssirssXi.,3;.. ..,:Sr.:S29A&s rS;,. .,  .5S. rh2:.r2SsrssiiirhA,  ,.        .@@     ")
(print"    #@,        ;S.  &HrrSsrsiiXS,.rHhs ;5;,;:....,:;r; ,9G:.r5295:  ,.,33,,S5sssSsissrAH  ,BM9X&Bi   ,@M     ")
(print"    G@;         i:  X#s;siiS25;  .r33h;,.:ihXi:,ir:,.r922,.SG52XG: ;Ms :. rGirSSiiiirsB3  :r    ,,   ;@9     ")
(print"    s@3   :i5SiXAr  r#2;rSS;:;  ri .2S,.,s2iX2::;:,,::,:5::3G23Gi .AB,  . :S2Sssrsii;XMr  rr         S@r     ")
(print"    :@#   :2Xs:.;i  .#ArsSs  ;&s ,s:  .r:sA32.,;.:2h3Xs,.;:;2r;;  :;.,2s::riissSssii;AM,  SHHHAGs.   A@:     ")
(print"     @@,      .. i;  X#Srr5S,.;r:. r9r.  :iSr.::i&93X225.      .,;;   .:SXXSssiisii;rM2       ,r;   ,@#      ")
(print"     s@X   ;S;iA9AS   #@rs529s, ,9S .r5S;,      ..     ,:;rrr;;;rr;.  :BX55issiisirr#&  .S@Xr,,:.   5@r      ")
(print"      @@,   iAS;:,:   s@h;SiiXh2;. .r;,,:;rrsr;;:,::;rrrs;,:;, ,iii5i:,22SissssiiSr2@r     :rG#i   .@@       ")
(print"      i@M     :..,,i:  s#5i5sii9X:.5HHh2r;::::;;;;r;;: .;rihBS,rHGX9M2 .25ssisssisiMs  ,i3Xr .r.   9@r       ")
(print"       @@:   ,r,  ;@X   2@issSSi:.shXSS3AAi:,:rr;;,.. :hH&9X3r ;3hSS2; .X2rsssssss#i  ,r. ,XA:    .@A        ")
(print"       ;@@     ;&M&;.,   X@s;sXs.   :r;;r: .r:,  ;i5S:,sisirr: ;r;...  .iSssiiirs#2   .sG:  ;;   .H@,        ")
(print"        X@9   .5i.   ;S.  r@&;;29;   .r;  rs:;i..SA9Gh.    ,. .r. :Sr..;92srii;2@s   r5:r3ii;    2@i         ")
(print"         #@i    :i   ,Hi   ;@@ir22i: ,r, .;;..:,;2XssXS, .,:, ,:,,,..;2XSsiSrs##:  ,iXM#5:.     r@#          ")
(terpri)
(terpri)
(print"                                    SISTEMA DE CONTROL DE CALIFICACIONES                                     ")
(terpri)
(terpri)
(terpri)
;(terpri)


  )

(defun main()
  (header)
 	(load-db "my-Subjects.db")
  	(load-db "my-Asistencias.db")
  	(load-db "my-Tareas.db")
  	(load-db "my-Examenes.db")
  	(load-db "my-Proyectos.db")
 ; (dump-db)
  	(menu)
  	(save-db "my-Subjects.db")
 	(save-db "my-Asistencias.db")
  	(save-db "my-Tareas.db")
  	(save-db "my-Examenes.db")
  	(save-db "my-Proyectos.db")
)

(main)



