;;Author: Daniel Izquierdo A.
;;Date: 01-11-15
;;
;;Reading and writing files in LISP
;;
;;
(defun Welcome()
  (terpri)
  (terpri)
  (write-line "r;rrrr;;;;;;;r@@@@@@@@@@@@@@@A,::::::::,.A@@@@@@@@@H;;;;;;;;;;;iB@@@@@@@@@@@@@@@@&r;;;;;;::::::::::::,..,rA@
r,;:::::::::,.@@@@@@@@@@@@@@@: .......  h@@@@@@@@#; .,,:,,.,rh@@@@@@@@@@@@@@@#X;,.,,,,,,,,,,.....    ,iM@@@@
r;rrr;;;;;;;:,@@@@@@@@@@@@@@A ,:::,,,..@@@@@@@@@;  .:::,:sA@@@@@@@@@@@@@@#9s:.  .:;;;;::::::,.. .;5#@@@@@@@@
r:rr;;;;;;;;:.@@@@@@@@@@@@@@                                        @@@#9s:.  .:;;;;::::::,.. .X@@@@@@@@@@@@")
  (write-line ";:r;;;;;;;;;: #@@@@@@@@@@@@B  WELCOME TO MY FIRST PROGRAM IN CLISP  ...,::;;;;;:::,.. .,;iH@@@@@@@@@@@@@@@@@")
  (write-line ";:;;;;;;;;;;: X@@@@@@@@@@@@;                                        :;;;;;;:,,...,:r2B@@@@@@@@@@@@@@@@@@@@@@
;:;;;;;;;;;;: ;@@@@@@@@@@@@  ,,,,. #@@@@@3    :A@@@@@&s,      .,,:::;;;;rrri59H@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
;:;;;;;;;;;;:, @@@@@@@@@@@# ..... 5@@@@@;   :#@@@Ms.  .:r59AB##@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
;:;;;;;;;;;;:, X@@@@@@@@@@h .,.. .@@@@@.  ,M@@@s,:r3#@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@#&")
  (terpri)
  (write-line "		+++++++++++++++++++++  by: Daniel Izquierdo  +++++++++++++++++++++")
  (terpri)
  (terpri)
  (menu)
  (terpri)
)

(defun menu()
  (loop do
    (loop do
      (write-line "what do you want to do?:")
      (terpri)
      (write-line "[1] enter my data")
      (write-line "[2] read file")
      (write-line "[3] exit")
      (setq op (read))
      while(or (> op 3)  (< op 1)))
     (if (/= op 3)
      (loop 
	(case op
	  (1 (return(pedir-datos)))
	  (2 (return(leer-datos)))
	 );;cierre del case
      );;cierre del loop
     );;cierre del if
     (terpri)
     (terpri)
     (terpri)
      while(/= op 3)
    );;cierre del loop do principal
);;cierre de funcion

(defun pedir-datos()
  (terpri)
  (princ "enter your full name: ")
  (setq name (read-line))
  (princ "email: ")
  (setq email (read-line))
  (with-open-file (stream "data.txt" :direction :output :if-exists :append)
    (format stream name)
    (format stream " ")
    (format stream email)
    (terpri stream)
  )
)

(defun leer-datos()
  ;;(write-line "muestro datos")
  (terpri)
  (let ((in (open "data.txt" :if-does-not-exist nil)))
    (when in
      (loop for line = (read-line in nil)
	while line do (format t "~a~%" line))
	(close in))
  )
  (terpri)
)




(welcome)