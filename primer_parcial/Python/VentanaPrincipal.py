# -*- coding: utf-8 -*-

# Form implementation generated from reading ui file 'VentanaPrincipal.ui'
#
# Created: Mon Dec 15 09:58:36 2014
#      by: PyQt4 UI code generator 4.11.3
#
# WARNING! All changes made in this file will be lost!

from PyQt4 import QtCore, QtGui

try:
    _fromUtf8 = QtCore.QString.fromUtf8
except AttributeError:
    def _fromUtf8(s):
        return s

try:
    _encoding = QtGui.QApplication.UnicodeUTF8
    def _translate(context, text, disambig):
        return QtGui.QApplication.translate(context, text, disambig, _encoding)
except AttributeError:
    def _translate(context, text, disambig):
        return QtGui.QApplication.translate(context, text, disambig)

class Ui_Dialog(object):
    def setupUi(self, Dialog):
        Dialog.setObjectName(_fromUtf8("Dialog"))
        Dialog.resize(606, 483)
        icon = QtGui.QIcon()
        icon.addPixmap(QtGui.QPixmap(_fromUtf8("../../../../../../../../Python27/DLLs/py.ico")), QtGui.QIcon.Normal, QtGui.QIcon.Off)
        Dialog.setWindowIcon(icon)
        Dialog.setLayoutDirection(QtCore.Qt.LeftToRight)
        Dialog.setAutoFillBackground(True)
        self.lbl1 = QtGui.QLabel(Dialog)
        self.lbl1.setGeometry(QtCore.QRect(10, 30, 591, 103))
        self.lbl1.setLayoutDirection(QtCore.Qt.LeftToRight)
        self.lbl1.setStyleSheet(_fromUtf8("font: 75 14pt \"Verdana\";"))
        self.lbl1.setAlignment(QtCore.Qt.AlignCenter)
        self.lbl1.setObjectName(_fromUtf8("lbl1"))
        self.groupBox = QtGui.QGroupBox(Dialog)
        self.groupBox.setGeometry(QtCore.QRect(80, 150, 421, 231))
        self.groupBox.setStyleSheet(_fromUtf8("font: italic 9pt \"Verdana\";\n"
"color: rgb(170, 0, 0);"))
        self.groupBox.setAlignment(QtCore.Qt.AlignCenter)
        self.groupBox.setObjectName(_fromUtf8("groupBox"))
        self.lineUsuario = QtGui.QLineEdit(self.groupBox)
        self.lineUsuario.setGeometry(QtCore.QRect(170, 60, 189, 22))
        self.lineUsuario.setStyleSheet(_fromUtf8("color:rgb(85, 170, 127);"))
        self.lineUsuario.setObjectName(_fromUtf8("lineUsuario"))
        self.lblUsuario = QtGui.QLabel(self.groupBox)
        self.lblUsuario.setGeometry(QtCore.QRect(20, 50, 138, 51))
        self.lblUsuario.setStyleSheet(_fromUtf8("font: 75 10pt \"Verdana\";"))
        self.lblUsuario.setObjectName(_fromUtf8("lblUsuario"))
        self.btnValidar = QtGui.QPushButton(self.groupBox)
        self.btnValidar.setGeometry(QtCore.QRect(310, 190, 93, 28))
        self.btnValidar.setStyleSheet(_fromUtf8("font: 9pt \"Verdana\";\n"
"color: rgb(0, 170, 0);"))
        self.btnValidar.setObjectName(_fromUtf8("btnValidar"))
        self.lblMensaje = QtGui.QLabel(Dialog)
        self.lblMensaje.setGeometry(QtCore.QRect(80, 110, 421, 20))
        self.lblMensaje.setStyleSheet(_fromUtf8("font: 87 9pt \"Segoe UI Black\";\n"
"color:rgb(255, 0, 0);"))
        self.lblMensaje.setText(_fromUtf8(""))
        self.lblMensaje.setObjectName(_fromUtf8("lblMensaje"))
        self.btnCrear = QtGui.QPushButton(Dialog)
        self.btnCrear.setGeometry(QtCore.QRect(170, 440, 93, 21))
        self.btnCrear.setObjectName(_fromUtf8("btnCrear"))
        self.label = QtGui.QLabel(Dialog)
        self.label.setGeometry(QtCore.QRect(72, 440, 91, 20))
        self.label.setObjectName(_fromUtf8("label"))

        self.retranslateUi(Dialog)
        QtCore.QMetaObject.connectSlotsByName(Dialog)

    def retranslateUi(self, Dialog):
        Dialog.setWindowTitle(_translate("Dialog", "LDPPython", None))
        self.lbl1.setText(_translate("Dialog", "<html><head/><body><p><span style=\" font-size:16pt; font-weight:600; color:#0055ff;\">Bienvenido al cajero Politécnico</span></p></body></html>", None))
        self.groupBox.setTitle(_translate("Dialog", "Autentificación", None))
        self.lblUsuario.setText(_translate("Dialog", "<html><head/><body><p align=\"center\"><span style=\" color:#55007f;\">Usuario</span></p></body></html>", None))
        self.btnValidar.setText(_translate("Dialog", "Siguiente", None))
        self.btnCrear.setText(_translate("Dialog", "Crear", None))
        self.label.setText(_translate("Dialog", "Nuevo Usuario?", None))

