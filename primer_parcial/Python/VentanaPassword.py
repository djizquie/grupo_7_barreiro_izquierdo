# -*- coding: utf-8 -*-

# Form implementation generated from reading ui file 'VentanaPassword.ui'
#
# Created: Wed Dec 17 16:25:29 2014
#      by: PyQt4 UI code generator 4.11.3
#
# WARNING! All changes made in this file will be lost!

from PyQt4 import QtCore, QtGui

try:
    _fromUtf8 = QtCore.QString.fromUtf8
except AttributeError:
    def _fromUtf8(s):
        return s

try:
    _encoding = QtGui.QApplication.UnicodeUTF8
    def _translate(context, text, disambig):
        return QtGui.QApplication.translate(context, text, disambig, _encoding)
except AttributeError:
    def _translate(context, text, disambig):
        return QtGui.QApplication.translate(context, text, disambig)

class Ui_ventanaPassword(object):
    def setupUi(self, ventanaPassword):
        ventanaPassword.setObjectName(_fromUtf8("ventanaPassword"))
        ventanaPassword.resize(616, 562)
        self.verticalLayoutWidget_5 = QtGui.QWidget(ventanaPassword)
        self.verticalLayoutWidget_5.setGeometry(QtCore.QRect(0, 20, 611, 488))
        self.verticalLayoutWidget_5.setObjectName(_fromUtf8("verticalLayoutWidget_5"))
        self.verticalLayout_6 = QtGui.QVBoxLayout(self.verticalLayoutWidget_5)
        self.verticalLayout_6.setMargin(0)
        self.verticalLayout_6.setObjectName(_fromUtf8("verticalLayout_6"))
        self.horizontalLayout = QtGui.QHBoxLayout()
        self.horizontalLayout.setObjectName(_fromUtf8("horizontalLayout"))
        self.verticalLayout = QtGui.QVBoxLayout()
        self.verticalLayout.setObjectName(_fromUtf8("verticalLayout"))
        spacerItem = QtGui.QSpacerItem(20, 40, QtGui.QSizePolicy.Minimum, QtGui.QSizePolicy.Expanding)
        self.verticalLayout.addItem(spacerItem)
        self.btn1 = QtGui.QPushButton(self.verticalLayoutWidget_5)
        self.btn1.setObjectName(_fromUtf8("btn1"))
        self.verticalLayout.addWidget(self.btn1)
        spacerItem1 = QtGui.QSpacerItem(20, 40, QtGui.QSizePolicy.Minimum, QtGui.QSizePolicy.Expanding)
        self.verticalLayout.addItem(spacerItem1)
        self.btn2 = QtGui.QPushButton(self.verticalLayoutWidget_5)
        self.btn2.setObjectName(_fromUtf8("btn2"))
        self.verticalLayout.addWidget(self.btn2)
        spacerItem2 = QtGui.QSpacerItem(20, 40, QtGui.QSizePolicy.Minimum, QtGui.QSizePolicy.Expanding)
        self.verticalLayout.addItem(spacerItem2)
        self.btn3 = QtGui.QPushButton(self.verticalLayoutWidget_5)
        self.btn3.setObjectName(_fromUtf8("btn3"))
        self.verticalLayout.addWidget(self.btn3)
        spacerItem3 = QtGui.QSpacerItem(20, 40, QtGui.QSizePolicy.Minimum, QtGui.QSizePolicy.Expanding)
        self.verticalLayout.addItem(spacerItem3)
        self.horizontalLayout.addLayout(self.verticalLayout)
        spacerItem4 = QtGui.QSpacerItem(40, 20, QtGui.QSizePolicy.Maximum, QtGui.QSizePolicy.Minimum)
        self.horizontalLayout.addItem(spacerItem4)
        self.verticalLayout_3 = QtGui.QVBoxLayout()
        self.verticalLayout_3.setObjectName(_fromUtf8("verticalLayout_3"))
        spacerItem5 = QtGui.QSpacerItem(20, 40, QtGui.QSizePolicy.Minimum, QtGui.QSizePolicy.Expanding)
        self.verticalLayout_3.addItem(spacerItem5)
        self.lbl1 = QtGui.QLabel(self.verticalLayoutWidget_5)
        self.lbl1.setStyleSheet(_fromUtf8("font: 75 8pt \"System\";\n"
"color:rgb(0, 170, 127);\n"
""))
        self.lbl1.setObjectName(_fromUtf8("lbl1"))
        self.verticalLayout_3.addWidget(self.lbl1)
        spacerItem6 = QtGui.QSpacerItem(20, 40, QtGui.QSizePolicy.Minimum, QtGui.QSizePolicy.Expanding)
        self.verticalLayout_3.addItem(spacerItem6)
        self.lbl2 = QtGui.QLabel(self.verticalLayoutWidget_5)
        self.lbl2.setStyleSheet(_fromUtf8("font: 75 8pt \"System\";\n"
"color:rgb(0, 170, 127);\n"
""))
        self.lbl2.setObjectName(_fromUtf8("lbl2"))
        self.verticalLayout_3.addWidget(self.lbl2)
        spacerItem7 = QtGui.QSpacerItem(20, 40, QtGui.QSizePolicy.Minimum, QtGui.QSizePolicy.Expanding)
        self.verticalLayout_3.addItem(spacerItem7)
        self.lbl3 = QtGui.QLabel(self.verticalLayoutWidget_5)
        self.lbl3.setStyleSheet(_fromUtf8("font: 75 8pt \"System\";\n"
"color:rgb(0, 170, 127);\n"
""))
        self.lbl3.setObjectName(_fromUtf8("lbl3"))
        self.verticalLayout_3.addWidget(self.lbl3)
        spacerItem8 = QtGui.QSpacerItem(20, 40, QtGui.QSizePolicy.Minimum, QtGui.QSizePolicy.Expanding)
        self.verticalLayout_3.addItem(spacerItem8)
        self.horizontalLayout.addLayout(self.verticalLayout_3)
        spacerItem9 = QtGui.QSpacerItem(40, 20, QtGui.QSizePolicy.Minimum, QtGui.QSizePolicy.Minimum)
        self.horizontalLayout.addItem(spacerItem9)
        self.verticalLayout_8 = QtGui.QVBoxLayout()
        self.verticalLayout_8.setObjectName(_fromUtf8("verticalLayout_8"))
        spacerItem10 = QtGui.QSpacerItem(20, 40, QtGui.QSizePolicy.Minimum, QtGui.QSizePolicy.Expanding)
        self.verticalLayout_8.addItem(spacerItem10)
        self.lbl4 = QtGui.QLabel(self.verticalLayoutWidget_5)
        self.lbl4.setLayoutDirection(QtCore.Qt.LeftToRight)
        self.lbl4.setStyleSheet(_fromUtf8("font: 75 8pt \"System\";\n"
"color:rgb(0, 170, 127);\n"
""))
        self.lbl4.setAlignment(QtCore.Qt.AlignRight|QtCore.Qt.AlignTrailing|QtCore.Qt.AlignVCenter)
        self.lbl4.setObjectName(_fromUtf8("lbl4"))
        self.verticalLayout_8.addWidget(self.lbl4)
        spacerItem11 = QtGui.QSpacerItem(20, 40, QtGui.QSizePolicy.Minimum, QtGui.QSizePolicy.Expanding)
        self.verticalLayout_8.addItem(spacerItem11)
        self.lbl5 = QtGui.QLabel(self.verticalLayoutWidget_5)
        self.lbl5.setStyleSheet(_fromUtf8("font: 75 8pt \"System\";\n"
"color:rgb(0, 170, 127);\n"
""))
        self.lbl5.setAlignment(QtCore.Qt.AlignRight|QtCore.Qt.AlignTrailing|QtCore.Qt.AlignVCenter)
        self.lbl5.setObjectName(_fromUtf8("lbl5"))
        self.verticalLayout_8.addWidget(self.lbl5)
        spacerItem12 = QtGui.QSpacerItem(20, 40, QtGui.QSizePolicy.Minimum, QtGui.QSizePolicy.Expanding)
        self.verticalLayout_8.addItem(spacerItem12)
        self.lbl6 = QtGui.QLabel(self.verticalLayoutWidget_5)
        self.lbl6.setStyleSheet(_fromUtf8("font: 75 8pt \"System\";\n"
"color:rgb(0, 170, 127);\n"
""))
        self.lbl6.setAlignment(QtCore.Qt.AlignRight|QtCore.Qt.AlignTrailing|QtCore.Qt.AlignVCenter)
        self.lbl6.setObjectName(_fromUtf8("lbl6"))
        self.verticalLayout_8.addWidget(self.lbl6)
        spacerItem13 = QtGui.QSpacerItem(20, 40, QtGui.QSizePolicy.Minimum, QtGui.QSizePolicy.Expanding)
        self.verticalLayout_8.addItem(spacerItem13)
        self.horizontalLayout.addLayout(self.verticalLayout_8)
        spacerItem14 = QtGui.QSpacerItem(40, 20, QtGui.QSizePolicy.Maximum, QtGui.QSizePolicy.Minimum)
        self.horizontalLayout.addItem(spacerItem14)
        self.verticalLayout_2 = QtGui.QVBoxLayout()
        self.verticalLayout_2.setObjectName(_fromUtf8("verticalLayout_2"))
        spacerItem15 = QtGui.QSpacerItem(20, 40, QtGui.QSizePolicy.Minimum, QtGui.QSizePolicy.Expanding)
        self.verticalLayout_2.addItem(spacerItem15)
        self.btn4 = QtGui.QPushButton(self.verticalLayoutWidget_5)
        self.btn4.setObjectName(_fromUtf8("btn4"))
        self.verticalLayout_2.addWidget(self.btn4)
        spacerItem16 = QtGui.QSpacerItem(20, 40, QtGui.QSizePolicy.Minimum, QtGui.QSizePolicy.Expanding)
        self.verticalLayout_2.addItem(spacerItem16)
        self.btn5 = QtGui.QPushButton(self.verticalLayoutWidget_5)
        self.btn5.setObjectName(_fromUtf8("btn5"))
        self.verticalLayout_2.addWidget(self.btn5)
        spacerItem17 = QtGui.QSpacerItem(20, 40, QtGui.QSizePolicy.Minimum, QtGui.QSizePolicy.Expanding)
        self.verticalLayout_2.addItem(spacerItem17)
        self.btn6 = QtGui.QPushButton(self.verticalLayoutWidget_5)
        self.btn6.setObjectName(_fromUtf8("btn6"))
        self.verticalLayout_2.addWidget(self.btn6)
        spacerItem18 = QtGui.QSpacerItem(20, 40, QtGui.QSizePolicy.Minimum, QtGui.QSizePolicy.Expanding)
        self.verticalLayout_2.addItem(spacerItem18)
        self.horizontalLayout.addLayout(self.verticalLayout_2)
        self.verticalLayout_6.addLayout(self.horizontalLayout)
        self.btnCancel = QtGui.QPushButton(ventanaPassword)
        self.btnCancel.setGeometry(QtCore.QRect(490, 520, 93, 28))
        self.btnCancel.setObjectName(_fromUtf8("btnCancel"))
        self.btnVerificar = QtGui.QPushButton(ventanaPassword)
        self.btnVerificar.setGeometry(QtCore.QRect(380, 520, 93, 28))
        self.btnVerificar.setObjectName(_fromUtf8("btnVerificar"))
        self.lblTimer = QtGui.QLabel(ventanaPassword)
        self.lblTimer.setGeometry(QtCore.QRect(20, 520, 241, 41))
        self.lblTimer.setText(_fromUtf8(""))
        self.lblTimer.setObjectName(_fromUtf8("lblTimer"))
        self.lblSms = QtGui.QLabel(ventanaPassword)
        self.lblSms.setGeometry(QtCore.QRect(130, 530, 221, 16))
        self.lblSms.setText(_fromUtf8(""))
        self.lblSms.setObjectName(_fromUtf8("lblSms"))

        self.retranslateUi(ventanaPassword)
        QtCore.QObject.connect(self.btnCancel, QtCore.SIGNAL(_fromUtf8("clicked()")), ventanaPassword.close)
        QtCore.QMetaObject.connectSlotsByName(ventanaPassword)

    def retranslateUi(self, ventanaPassword):
        ventanaPassword.setWindowTitle(_translate("ventanaPassword", "Password", None))
        self.btn1.setText(_translate("ventanaPassword", "boton 1", None))
        self.btn2.setText(_translate("ventanaPassword", "boton 2", None))
        self.btn3.setText(_translate("ventanaPassword", "boton 3", None))
        self.lbl1.setText(_translate("ventanaPassword", "<html><head/><body><p>cambiar</p></body></html>", None))
        self.lbl2.setText(_translate("ventanaPassword", "<html><head/><body><p>cambiar</p></body></html>", None))
        self.lbl3.setText(_translate("ventanaPassword", "<html><head/><body><p>cambiar</p></body></html>", None))
        self.lbl4.setText(_translate("ventanaPassword", "<html><head/><body><p>cambiar</p></body></html>", None))
        self.lbl5.setText(_translate("ventanaPassword", "<html><head/><body><p>cambiar</p></body></html>", None))
        self.lbl6.setText(_translate("ventanaPassword", "<html><head/><body><p>cambiar</p></body></html>", None))
        self.btn4.setText(_translate("ventanaPassword", "boton 4", None))
        self.btn5.setText(_translate("ventanaPassword", "boton 5", None))
        self.btn6.setText(_translate("ventanaPassword", "boton 6", None))
        self.btnCancel.setText(_translate("ventanaPassword", "Cancelar", None))
        self.btnVerificar.setText(_translate("ventanaPassword", "Verificar", None))

#import recursos_rc
