from random import choice
from random import randrange


def generarSecuenciaAleatoria(clave):

	
	valores = ""
	consonantes = ""
	vocales = ""
	cont = 4
	
	digito = 0
	for a in range(4):
		digito = clave % 10
		clave /= 10
		if digito == 0:
			valores += "ceroCERO"
			consonantes += "crCR"
			vocales += "eoEO"
		elif digito == 1:
			valores += "unoUNO"
			consonantes += "nN"
			vocales += "uoUO"
		elif digito == 2:
			valores += "dosDOS"
			consonantes += "dsDS"
			vocales += "oO"
		elif digito == 3:
			valores += "tresTRES"
			consonantes += "trsTRS"
			vocales += "eE"
		elif digito == 4:
			valores += "cuatroCUATRO"
			consonantes += "ctrCTR"
			vocales += "uaoUAO"
		elif digito == 5:
			valores += "cincoCINCO"
			consonantes += "cnCN"
			vocales += "ioIO"
		elif digito == 6:
			valores += "seisSEIS"
			consonantes += "sS"
			vocales += "eiEI"
		elif digito == 7:
			valores += "sieteSIETE"
			consonantes += "stST"
			vocales += "ieIE"
		elif digito == 8:
			valores += "ochoOCHO"
			consonantes += "chCH"
			vocales += "oO"
		else :
			valores += "nueveNUEVE"
			consonantes += "nvNV"
			vocales += "ueUE"

	a =4 
	lg=[1,2]
	secuenciaUsuario = []
	p = ""
	
	while a>0:
		al = range(choice(lg))
		if (len (al) == 2):
			p = p.join(choice(consonantes)) + p.join(choice(vocales))
		else:
			if (randrange(2) == 0):
				p = p.join(choice(consonantes))
			else:
				p = p.join(choice(vocales))

		#print p
		secuenciaUsuario.append(p)
		p = ""
		a-=1
	return secuenciaUsuario


