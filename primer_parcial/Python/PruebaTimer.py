# -*- coding: utf-8 -*-

# Form implementation generated from reading ui file 'PruebaTimer.ui'
#
# Created: Wed Dec 17 08:30:51 2014
#      by: PyQt4 UI code generator 4.11.3
#
# WARNING! All changes made in this file will be lost!

from PyQt4 import QtCore, QtGui

try:
    _fromUtf8 = QtCore.QString.fromUtf8
except AttributeError:
    def _fromUtf8(s):
        return s

try:
    _encoding = QtGui.QApplication.UnicodeUTF8
    def _translate(context, text, disambig):
        return QtGui.QApplication.translate(context, text, disambig, _encoding)
except AttributeError:
    def _translate(context, text, disambig):
        return QtGui.QApplication.translate(context, text, disambig)

class Ui_centroFormulario(object):
    def setupUi(self, centroFormulario):
        centroFormulario.setObjectName(_fromUtf8("centroFormulario"))
        centroFormulario.resize(400, 300)
        self.lblTimer = QtGui.QLabel(centroFormulario)
        self.lblTimer.setGeometry(QtCore.QRect(70, 40, 53, 16))
        self.lblTimer.setObjectName(_fromUtf8("lblTimer"))

        self.retranslateUi(centroFormulario)
        QtCore.QMetaObject.connectSlotsByName(centroFormulario)

    def retranslateUi(self, centroFormulario):
        centroFormulario.setWindowTitle(_translate("centroFormulario", "Form", None))
        self.lblTimer.setText(_translate("centroFormulario", "TextLabel", None))

