# -*- coding: utf-8 -*-

# Form implementation generated from reading ui file 'userSesion.ui'
#
# Created: Mon Dec 15 01:34:39 2014
#      by: PyQt4 UI code generator 4.11.3
#
# WARNING! All changes made in this file will be lost!

from PyQt4 import QtCore, QtGui

try:
    _fromUtf8 = QtCore.QString.fromUtf8
except AttributeError:
    def _fromUtf8(s):
        return s

try:
    _encoding = QtGui.QApplication.UnicodeUTF8
    def _translate(context, text, disambig):
        return QtGui.QApplication.translate(context, text, disambig, _encoding)
except AttributeError:
    def _translate(context, text, disambig):
        return QtGui.QApplication.translate(context, text, disambig)

class Ui_userSesion(object):
    def setupUi(self, userSesion):
        userSesion.setObjectName(_fromUtf8("userSesion"))
        userSesion.resize(436, 356)
        self.lineName = QtGui.QLineEdit(userSesion)
        self.lineName.setEnabled(False)
        self.lineName.setGeometry(QtCore.QRect(190, 70, 161, 22))
        self.lineName.setObjectName(_fromUtf8("lineName"))
        self.lineDNI = QtGui.QLineEdit(userSesion)
        self.lineDNI.setEnabled(False)
        self.lineDNI.setGeometry(QtCore.QRect(190, 130, 161, 22))
        self.lineDNI.setObjectName(_fromUtf8("lineDNI"))
        self.label_5 = QtGui.QLabel(userSesion)
        self.label_5.setGeometry(QtCore.QRect(70, 130, 53, 16))
        self.label_5.setObjectName(_fromUtf8("label_5"))
        self.lineMonto = QtGui.QLineEdit(userSesion)
        self.lineMonto.setEnabled(False)
        self.lineMonto.setGeometry(QtCore.QRect(190, 160, 161, 22))
        self.lineMonto.setObjectName(_fromUtf8("lineMonto"))
        self.lineNumber = QtGui.QLineEdit(userSesion)
        self.lineNumber.setEnabled(False)
        self.lineNumber.setGeometry(QtCore.QRect(190, 100, 161, 22))
        self.lineNumber.setObjectName(_fromUtf8("lineNumber"))
        self.label_2 = QtGui.QLabel(userSesion)
        self.label_2.setGeometry(QtCore.QRect(70, 70, 53, 16))
        self.label_2.setObjectName(_fromUtf8("label_2"))
        self.label_4 = QtGui.QLabel(userSesion)
        self.label_4.setGeometry(QtCore.QRect(70, 160, 121, 16))
        self.label_4.setObjectName(_fromUtf8("label_4"))
        self.label_3 = QtGui.QLabel(userSesion)
        self.label_3.setGeometry(QtCore.QRect(70, 100, 111, 16))
        self.label_3.setObjectName(_fromUtf8("label_3"))
        self.pushButton = QtGui.QPushButton(userSesion)
        self.pushButton.setGeometry(QtCore.QRect(200, 310, 93, 28))
        self.pushButton.setObjectName(_fromUtf8("pushButton"))
        self.pushButton_2 = QtGui.QPushButton(userSesion)
        self.pushButton_2.setGeometry(QtCore.QRect(310, 310, 93, 28))
        self.pushButton_2.setObjectName(_fromUtf8("pushButton_2"))

        self.retranslateUi(userSesion)
        QtCore.QObject.connect(self.pushButton_2, QtCore.SIGNAL(_fromUtf8("clicked()")), userSesion.close)
        QtCore.QMetaObject.connectSlotsByName(userSesion)

    def retranslateUi(self, userSesion):
        userSesion.setWindowTitle(_translate("userSesion", "Sesion", None))
        self.label_5.setText(_translate("userSesion", "cedula", None))
        self.label_2.setText(_translate("userSesion", "nombre", None))
        self.label_4.setText(_translate("userSesion", "monto en la cuenta", None))
        self.label_3.setText(_translate("userSesion", "numero de cuenta", None))
        self.pushButton.setText(_translate("userSesion", "refrescar", None))
        self.pushButton_2.setText(_translate("userSesion", "salir", None))

