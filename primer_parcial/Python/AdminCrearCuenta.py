# -*- coding: utf-8 -*-

# Form implementation generated from reading ui file 'AdminCrearCuenta.ui'
#
# Created: Sun Dec 14 09:48:32 2014
#      by: PyQt4 UI code generator 4.11.3
#
# WARNING! All changes made in this file will be lost!

from PyQt4 import QtCore, QtGui

try:
    _fromUtf8 = QtCore.QString.fromUtf8
except AttributeError:
    def _fromUtf8(s):
        return s

try:
    _encoding = QtGui.QApplication.UnicodeUTF8
    def _translate(context, text, disambig):
        return QtGui.QApplication.translate(context, text, disambig, _encoding)
except AttributeError:
    def _translate(context, text, disambig):
        return QtGui.QApplication.translate(context, text, disambig)

class Ui_ClaveCrearCuenta(object):
    def setupUi(self, ClaveCrearCuenta):
        ClaveCrearCuenta.setObjectName(_fromUtf8("ClaveCrearCuenta"))
        ClaveCrearCuenta.resize(389, 214)
        self.label = QtGui.QLabel(ClaveCrearCuenta)
        self.label.setGeometry(QtCore.QRect(50, 80, 121, 16))
        self.label.setObjectName(_fromUtf8("label"))
        self.lineAdmin = QtGui.QLineEdit(ClaveCrearCuenta)
        self.lineAdmin.setGeometry(QtCore.QRect(190, 80, 113, 22))
        self.lineAdmin.setContextMenuPolicy(QtCore.Qt.DefaultContextMenu)
        self.lineAdmin.setEchoMode(QtGui.QLineEdit.Password)
        self.lineAdmin.setObjectName(_fromUtf8("lineAdmin"))
        self.btnAdmin = QtGui.QPushButton(ClaveCrearCuenta)
        self.btnAdmin.setGeometry(QtCore.QRect(210, 140, 93, 28))
        self.btnAdmin.setObjectName(_fromUtf8("btnAdmin"))

        self.retranslateUi(ClaveCrearCuenta)
        QtCore.QMetaObject.connectSlotsByName(ClaveCrearCuenta)

    def retranslateUi(self, ClaveCrearCuenta):
        ClaveCrearCuenta.setWindowTitle(_translate("ClaveCrearCuenta", "Dialog", None))
        self.label.setText(_translate("ClaveCrearCuenta", "clave Administrador", None))
        self.btnAdmin.setText(_translate("ClaveCrearCuenta", "ok", None))

