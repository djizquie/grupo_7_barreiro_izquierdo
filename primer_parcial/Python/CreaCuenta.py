# -*- coding: utf-8 -*-

# Form implementation generated from reading ui file 'formCreaCuenta.ui'
#
# Created: Sun Dec 14 12:15:41 2014
#      by: PyQt4 UI code generator 4.11.3
#
# WARNING! All changes made in this file will be lost!

from PyQt4 import QtCore, QtGui

try:
    _fromUtf8 = QtCore.QString.fromUtf8
except AttributeError:
    def _fromUtf8(s):
        return s

try:
    _encoding = QtGui.QApplication.UnicodeUTF8
    def _translate(context, text, disambig):
        return QtGui.QApplication.translate(context, text, disambig, _encoding)
except AttributeError:
    def _translate(context, text, disambig):
        return QtGui.QApplication.translate(context, text, disambig)

class Ui_CrCuent(object):
    def setupUi(self, CrCuent):
        CrCuent.setObjectName(_fromUtf8("CrCuent"))
        CrCuent.resize(404, 345)
        self.label = QtGui.QLabel(CrCuent)
        self.label.setGeometry(QtCore.QRect(100, 10, 151, 16))
        self.label.setObjectName(_fromUtf8("label"))
        self.label_2 = QtGui.QLabel(CrCuent)
        self.label_2.setGeometry(QtCore.QRect(40, 70, 53, 16))
        self.label_2.setObjectName(_fromUtf8("label_2"))
        self.label_3 = QtGui.QLabel(CrCuent)
        self.label_3.setGeometry(QtCore.QRect(40, 100, 111, 16))
        self.label_3.setObjectName(_fromUtf8("label_3"))
        self.label_4 = QtGui.QLabel(CrCuent)
        self.label_4.setGeometry(QtCore.QRect(40, 160, 121, 16))
        self.label_4.setObjectName(_fromUtf8("label_4"))
        self.label_5 = QtGui.QLabel(CrCuent)
        self.label_5.setGeometry(QtCore.QRect(40, 130, 53, 16))
        self.label_5.setObjectName(_fromUtf8("label_5"))
        self.lineName = QtGui.QLineEdit(CrCuent)
        self.lineName.setGeometry(QtCore.QRect(160, 70, 113, 22))
        self.lineName.setObjectName(_fromUtf8("lineName"))
        self.lineNumber = QtGui.QLineEdit(CrCuent)
        self.lineNumber.setGeometry(QtCore.QRect(160, 100, 113, 22))
        self.lineNumber.setObjectName(_fromUtf8("lineNumber"))
        self.lineDNI = QtGui.QLineEdit(CrCuent)
        self.lineDNI.setGeometry(QtCore.QRect(160, 130, 113, 22))
        self.lineDNI.setObjectName(_fromUtf8("lineDNI"))
        self.lineMonto = QtGui.QLineEdit(CrCuent)
        self.lineMonto.setGeometry(QtCore.QRect(160, 160, 113, 22))
        self.lineMonto.setObjectName(_fromUtf8("lineMonto"))
        self.pushButton = QtGui.QPushButton(CrCuent)
        self.pushButton.setGeometry(QtCore.QRect(140, 290, 93, 28))
        self.pushButton.setObjectName(_fromUtf8("pushButton"))
        self.btnCrear = QtGui.QPushButton(CrCuent)
        self.btnCrear.setGeometry(QtCore.QRect(260, 290, 93, 28))
        self.btnCrear.setObjectName(_fromUtf8("btnCrear"))
        self.label_6 = QtGui.QLabel(CrCuent)
        self.label_6.setGeometry(QtCore.QRect(40, 210, 111, 16))
        self.label_6.setObjectName(_fromUtf8("label_6"))
        self.label_7 = QtGui.QLabel(CrCuent)
        self.label_7.setGeometry(QtCore.QRect(40, 240, 111, 16))
        self.label_7.setObjectName(_fromUtf8("label_7"))
        self.line = QtGui.QFrame(CrCuent)
        self.line.setGeometry(QtCore.QRect(0, 180, 401, 20))
        self.line.setFrameShape(QtGui.QFrame.HLine)
        self.line.setFrameShadow(QtGui.QFrame.Sunken)
        self.line.setObjectName(_fromUtf8("line"))
        self.txtClaveA = QtGui.QTextBrowser(CrCuent)
        self.txtClaveA.setGeometry(QtCore.QRect(150, 240, 191, 20))
        self.txtClaveA.setObjectName(_fromUtf8("txtClaveA"))
        self.txtClaveN = QtGui.QTextBrowser(CrCuent)
        self.txtClaveN.setGeometry(QtCore.QRect(150, 210, 191, 20))
        self.txtClaveN.setObjectName(_fromUtf8("txtClaveN"))

        self.retranslateUi(CrCuent)
        QtCore.QObject.connect(self.pushButton, QtCore.SIGNAL(_fromUtf8("clicked()")), self.lineName.clear)
        QtCore.QObject.connect(self.pushButton, QtCore.SIGNAL(_fromUtf8("clicked()")), self.lineNumber.clear)
        QtCore.QObject.connect(self.pushButton, QtCore.SIGNAL(_fromUtf8("clicked()")), self.lineDNI.clear)
        QtCore.QObject.connect(self.pushButton, QtCore.SIGNAL(_fromUtf8("clicked()")), self.lineMonto.clear)
        QtCore.QObject.connect(self.pushButton, QtCore.SIGNAL(_fromUtf8("clicked()")), self.txtClaveN.clear)
        QtCore.QObject.connect(self.pushButton, QtCore.SIGNAL(_fromUtf8("clicked()")), self.txtClaveA.clear)
        QtCore.QMetaObject.connectSlotsByName(CrCuent)

    def retranslateUi(self, CrCuent):
        CrCuent.setWindowTitle(_translate("CrCuent", "Dialog", None))
        self.label.setText(_translate("CrCuent", "Creación de Nueva Cuenta", None))
        self.label_2.setText(_translate("CrCuent", "nombre", None))
        self.label_3.setText(_translate("CrCuent", "numero de cuenta", None))
        self.label_4.setText(_translate("CrCuent", "monto en la cuenta", None))
        self.label_5.setText(_translate("CrCuent", "cedula", None))
        self.pushButton.setText(_translate("CrCuent", "limpiar", None))
        self.btnCrear.setText(_translate("CrCuent", "Crear", None))
        self.label_6.setText(_translate("CrCuent", "clave numerica", None))
        self.label_7.setText(_translate("CrCuent", "clave Bancomatico", None))

