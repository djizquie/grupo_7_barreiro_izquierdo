# -*- encoding: utf-8 -*-
import sys

from VentanaPassword import *
from AdminCrearCuenta import *
from CreaCuenta import *
from VentanaPrincipal import *
from Generacion import *
from userSesion import *
from random import randrange
import time
import string
from temporizador import *
from PruebaTimer import *
import os
from threading import Thread, Timer

###########################################################################################################
##	Clase de el formulario principal																	###
##																										###
###########################################################################################################
class MiFormulario(QtGui.QDialog):
	def __init__(self,parent=None):
		QtGui.QWidget.__init__(self,parent)
		self.ui= Ui_Dialog()
		self.ui.setupUi(self)
		self.user=""
		self.usrcomp=""
		#print(self.user+"<-- user fr1?")
		
		#QtCore.QObject.connect(self.ui.btnValidar, QtCore.SIGNAL('clicked()'), self.mostrarmensaje)
		#QtCore.QObject.connect(self.ui.btnValidar, QtCore.SIGNAL('clicked()'), self.on_btnValidar_clicked)
		#QtCore.QObject.connect(self.ui.btnCrear, QtCore.SIGNAL('clicked()'), self.on_btnCrear_clicked)
		self.ui.btnValidar.clicked.connect(self.on_btnValidar_clicked)
		self.ui.btnCrear.clicked.connect(self.on_btnCrear_clicked)
		self.user=""
		self.pssfrm=None
		self.admcc=None
		
		
	#def ventanaPassword(self,secuencia):
	#	VentanaPassword.__init__(self,secuencia)
	
	###################################################################################
	##utiliza mostrarMensaje() para verificar que ese usuario
	## se encuentra registrado y luego llama al formulario para ingresar el password
	#####################################################################################
	def on_btnValidar_clicked(self):
		self.user=str(self.ui.lineUsuario.text())
		if self.mostrarmensaje()==1:
			self.ui.lineUsuario.setText("")
			self.pssfrm=PassForm(self)
			self.pssfrm.exec_()
	###############################################
	##llama al formulario para ingresar la clave
	##administrativa para ingresar un nuevo user
	###############################################
	def on_btnCrear_clicked(self):
		self.ui.lineUsuario.setText("")
		self.admcc=ClaveAdminCrea()
		self.admcc.exec_()
	###################################################
	##verifica si el usuario se encuentra registrado ##
	##y envia una bandera							 ##
	###################################################
	def mostrarmensaje(self):
		arch= open("Usuarios.txt","r")
		nameUser=""
		flag=0
		c=""
		i=0
		self.usrcomp=""
		usr=self.ui.lineUsuario.text()
		op=open("Usuarios.txt","r")
		for line in op:
			if self.user!=line.split(' ')[0]:
				pass
			else:
				print(line.split(' ')[5].split('_'))
				
				self.usrcomp=str(line.split(' ')[0])
				print(self.usrcomp)
				self.ui.lblMensaje.setText("")
				flag=1
				break
		#for line in arch:
			#for c in line:
				
			#	if c != " ":
			#		nameUser+=c;
			#		
			#	else:
			#		
				#	if nameUser == usr:
				#		flag=1
				#		usrcomp=nameUser
				#		self.ui.lblMensaje.setText("")
				#		nameUser=""
				#		break
				#	else:
				#		nameUser=""
				#		break
			if flag == 1:
				break
			
		if self.user!=self.usrcomp:			
			self.ui.lblMensaje.setText("usuario equivocado o no existe")
			
		arch.close()
		return flag;# 1 si se encuentra usuario y 0 si no se encuentra
#################################################################################################################################	
class ClaveAdminCrea(QtGui.QDialog):
	def __init__(self,parent=None):
		QtGui.QWidget.__init__(self,parent)
		self.ui= Ui_ClaveCrearCuenta()
		self.ui.setupUi(self)
		self.ui.btnAdmin.clicked.connect(self.on_btnAdmin_clicked)
		
		self.crearCuen=None
		
	####################################
	##verifica si la clave de administrador es correcta
	##y abre el formulario para ingresar un nuevo user
	####################################
	def on_btnAdmin_clicked(self):
		psadm=self.ui.lineAdmin.text()
		self.ui.lineAdmin.clear()
		fo = open("passAdmin.txt", "r")
		self.crearCuen=CrearCuenta()
		if psadm==fo.readline():
			self.crearCuen.exec_()
	
#####################################################################################################################################					
#####################################################################################################################################
class CrearCuenta(QtGui.QDialog):
	def __init__(self,parent=None):
		QtGui.QWidget.__init__(self,parent)
		self.ui= Ui_CrCuent()
		self.ui.setupUi(self)
		self.ui.btnCrear.clicked.connect(self.on_btnCrear_clicked)
	############################################################################
	##genera una clave numerica aleatoria y su respectiva mascara alfabetica
	##ademas guarda en el archivo de usuarios el nuevo usuario
	############################################################################
	def on_btnCrear_clicked(self):
		clave = randrange(1000, 9999)
		secuencia=generarSecuenciaAleatoria(clave)
		self.ui.txtClaveN.setText(str(clave))
		self.ui.txtClaveA.setText(' '.join(secuencia))
		nombre=self.ui.lineName.text()
		cuenta=self.ui.lineNumber.text()
		cedula=self.ui.lineDNI.text()
		monto=self.ui.lineMonto.text()
		
		fo = open("Usuarios.txt", "a")
		fo.write(nombre + " " + str(cuenta) + " " + cedula + " " + str(monto) + " " +str(clave)+" ")
		for s in secuencia:
			fo.write("_"+str(s))
		
		fo.write("_")
		fo.write("\n")
		fo.close()
#####################################################################################################################################	
class UserSesion(QtGui.QDialog):
	def __init__(self,pssfrm,parent=None):
		QtGui.QWidget.__init__(self,parent)
		self.ui= Ui_userSesion()
		self.ui.setupUi(self)
		self.usr=pssfrm.user
		self.nCuenta=""
		self.DNI=""
		self.monto=""
		self.ui.pushButton.clicked.connect(self.on_pushButton_clicked)
	
	######################################
	##refresca los datos del usuario 
	##para que puedan ser visualizados
	####################################
	def on_pushButton_clicked(self):
		op=open("Usuarios.txt","r")
		for line in op:
			print(line.split(' ')[0]+" diferentes? "+self.usr)
			#for pal in line.split(' '):#separa en lista de palabras separadas #### formato: nameUser noCuenta cedula monto _p1_p2_p3_p4_
			if self.usr!=line.split(' ')[0]:
				pass
			else:
				self.nCuenta=line.split(' ')[1]
				self.DNI=line.split(' ')[2]
				self.monto=line.split(' ')[3]
		print (self.nCuenta +" D; "+self.DNI+" D; "+self.monto)
		self.ui.lineName.setText(self.usr)
		self.ui.lineNumber.setText(self.nCuenta)
		self.ui.lineDNI.setText(self.DNI)
		self.ui.lineMonto.setText(self.monto)
	#def mostrarDatos(self):
##########################################################################################################################
#####################################################################################################################################		
class PassForm(QtGui.QDialog):

	def __init__(self,form1,parent=None):#,user#):
		QtGui.QWidget.__init__(self,parent)
		self.user=form1.user
		print(self.user+"<--user?")
		self.ui= Ui_ventanaPassword()
		self.ui.setupUi(self)
		self.pssw=self.obtenersecuencia()#lista de password del usuario correcta
		#print(self.obtenersecuencia(self.user))
		self.llenarBotones(self.obtenersecuencia())
		self.ls=[]
		#print (self.botones_presionados)
		
		
		self.ui.btn1.clicked.connect(self.buttonClicked)
		self.ui.btn2.clicked.connect(self.buttonClicked)
		self.ui.btn3.clicked.connect(self.buttonClicked)
		self.ui.btn4.clicked.connect(self.buttonClicked)
		self.ui.btn5.clicked.connect(self.buttonClicked)
		self.ui.btn6.clicked.connect(self.buttonClicked)
		
		self.ui.btnVerificar.clicked.connect(self.verifClicked)
		self.Sesion=None
		self.ctimer=30
		#self.Thread=Thread(target =self.clock_timer,args = ())
		if self.underMouse()==False:
			print ":)"
			#self.Thread.start()
	
	
	#def start_timer(self):
    
	#	self.thread = Thread(target =self.clock_timer,args = (self))
	#	self.thread.start()
################################################
## método que implementa el temporizador a
## utilizarse en la ventana de ingresar el password
################################################
	def clock_timer(self):
    
		
		while self.ctimer:
        
			self.ctimer -= 1
			time.sleep(1)
        
			if self.underMouse(): 
				self.ctimer =30
				self.ui.lblTimer.setText("")
				
			else:
				self.ui.lblTimer.setText(str(self.ctimer))
				if self.ctimer==0:
			
					self.close()
	####################################
	##verifica si la contrasena de la mascara alfabetica
	##es correcta armando una lista y comparandola con otra
	####################################
	def verifClicked(self):
		ps=self.pssw
		comp=self.ls
		cont=0
		fl1=[]
		#print (comp)
		#print(ps)
		for i in range(len(ps)):
			for j in range (len(comp)):
				if ps[i]==comp[i][j]:
					cont+=1
					fl1.append(comp[i][j])
					break
					
		cmmp=self.sonListasIguales(ps,fl1)
		if cmmp==True:
			self.Sesion=UserSesion(self)
			self.hide()
			self.Sesion.exec_()
			self.close()#############
		else:
			print("D; no son iguales")
	####################################
	##comprueba si dos listas son iguales
	##
	####################################	
	def sonListasIguales(self,lista_a,lista_b):
		lista_a.sort(cmp=None, key=None, reverse=False)
		lista_b.sort(cmp=None, key=None, reverse=False)
 
		if len(lista_a) != len(lista_b):
			return False
		else:
			for i in range(0,len(lista_a)):
				if(lista_a[i] != lista_b[i]):
					return False
 
		return True
		
		#QtCore.QObject.connect(self.ui.btn1, QtCore.SIGNAL('clicked()'), self.mostrarmensaje) <------ con esto se imprimia en el boton 1 la cadena de clave del usuario
		
	######################################################
	##llena los labels correspondientes a los botones
	##que contienen la mascara alfabetica en la pantalla
	#######################################################
	def llenarBotones(self,pss):
		##lista de listas
		##btn1 [""][""][""][""]		[""][""][""][""] btn4
		##btn2 [""][""][""][""]		[""][""][""][""] btn5
		##btn3 [""][""][""][""]		[""][""][""][""] btn6
		####################################################################################################################################3
		numero_filas=6				####################################################################################3
		numero_columnas=4			############
		btns = [None] * numero_filas############ Codigo tomado y adaptado de: http://www.cristalab.com/tutoriales/crear-matrices-en-python-utilizando-listas-c103122l/
		for i in range(numero_filas):###########
			btns[i] = [None] * numero_columnas########################################################################################
		####################################################################################################################################
		#print(btns)
		plbr="abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ"
		print(pss)
		print("<---password usuario")
		for i in range (numero_columnas):
			n1=randrange(0, 5)
			n2=randrange(0,3)
			while btns[n1][n2]!=None:
				n1=randrange(0, 5)
				n2=randrange(0,3)
			btns[n1][n2]=pss[i]
		for i in range (numero_filas):
			for j in range (numero_columnas):
				if btns[i][j]==None:
					btns[i][j]=plbr[randrange(0,len(plbr))]+plbr[randrange(0,len(plbr))]
		#print(btns)
		self.ui.lbl1.setText(' '.join(btns[0]))
		self.ui.lbl2.setText(' '.join(btns[1]))
		self.ui.lbl3.setText(' '.join(btns[2]))
		self.ui.lbl4.setText(' '.join(btns[3]))
		self.ui.lbl5.setText(' '.join(btns[4]))
		self.ui.lbl6.setText(' '.join(btns[5]))
	################################################################
	##obtiene el contenido de los labels de
	##los botones q van siendo presionados
	##y controla q sean solo 4 los q pueden ser presionados
	##si son mas de 4 la clave es erronea y se cierra el formulario
	################################################################
	def buttonClicked(self):
		sender=self.sender()
		if len(self.ls)<4:
			if sender.objectName()=="btn1":
				self.ls.append (str(self.ui.lbl1.text()).split(' '))
				print(1)
			if sender.objectName()=="btn2":
				self.ls.append (str(self.ui.lbl2.text()).split(' '))
				print(2)
			if sender.objectName()=="btn3":
				self.ls.append (str(self.ui.lbl3.text()).split(' '))
				print(3)
			if sender.objectName()=="btn4":
				self.ls.append (str(self.ui.lbl4.text()).split(' '))
				print(4)
			if sender.objectName()=="btn5":
				self.ls.append (str(self.ui.lbl5.text()).split(' '))
				print(5)
			if sender.objectName()=="btn6":
				self.ls.append (str(self.ui.lbl6.text()).split(' '))
				print(6)
		else:
			self.ui.lblSms.setText("mas de 4 botones pulsados! cerrando..")
			print(self.ls)
			time.sleep(1)
			self.close()
			
 
	##############################################################################
	##obtiene la secuencia de la mascara alfabetica correspondiente al usuario
	##############################################################################
	def obtenersecuencia(self):
		op=open("Usuarios.txt","r")
		for line in op:
			#print(line.split(' ')[0]+" diferentes? "+self.usr)
			#for pal in line.split(' '):#separa en lista de palabras separadas #### formato: nameUser noCuenta cedula monto _p1_p2_p3_p4_
			if self.user!=line.split(' ')[0]:
				pass
			else:
				print("resutl obtener secuencia passform")
				li=line.split(' ')[5].split('_')
				li.remove('')
				li.remove('\n')
				return li
		
	
	

##########################################################################################################################################
if __name__ == '__main__':
	app = QtGui.QApplication(sys.argv)
	myapp = MiFormulario()
	myapp.show()
	
	sys.exit(app.exec_())