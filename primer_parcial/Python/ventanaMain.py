from PyQt4 import QtCore, QtGui

try:
    _fromUtf8 = QtCore.QString.fromUtf8
except AttributeError:
    def _fromUtf8(s):
        return s

try:
    _encoding = QtGui.QApplication.UnicodeUTF8
    def _translate(context, text, disambig):
        return QtGui.QApplication.translate(context, text, disambig, _encoding)
except AttributeError:
    def _translate(context, text, disambig):
        return QtGui.QApplication.translate(context, text, disambig)

class Ui_Dialog(object):
    def setupUi(self, Dialog):
        Dialog.setObjectName(_fromUtf8("Dialog"))
        Dialog.resize(606, 483)
        icon = QtGui.QIcon()
        icon.addPixmap(QtGui.QPixmap(_fromUtf8("../../../../../../../../Python27/DLLs/py.ico")), QtGui.QIcon.Normal, QtGui.QIcon.Off)
        Dialog.setWindowIcon(icon)
        Dialog.setLayoutDirection(QtCore.Qt.LeftToRight)
        Dialog.setAutoFillBackground(True)
        self.lbl1 = QtGui.QLabel(Dialog)
        self.lbl1.setGeometry(QtCore.QRect(10, 20, 591, 103))
        self.lbl1.setLayoutDirection(QtCore.Qt.LeftToRight)
        self.lbl1.setStyleSheet(_fromUtf8("font: 75 14pt \"Verdana\";"))
        self.lbl1.setAlignment(QtCore.Qt.AlignCenter)
        self.lbl1.setObjectName(_fromUtf8("lbl1"))
        self.groupBox = QtGui.QGroupBox(Dialog)
        self.groupBox.setGeometry(QtCore.QRect(80, 150, 421, 231))
        self.groupBox.setStyleSheet(_fromUtf8("font: italic 9pt \"Verdana\";\n"
"color: rgb(170, 0, 0);"))
        self.groupBox.setAlignment(QtCore.Qt.AlignCenter)
        self.groupBox.setObjectName(_fromUtf8("groupBox"))
        self.lineUsuario = QtGui.QLineEdit(self.groupBox)
        self.lineUsuario.setGeometry(QtCore.QRect(170, 60, 189, 22))
        self.lineUsuario.setStyleSheet(_fromUtf8("color:rgb(85, 170, 127);"))
        self.lineUsuario.setObjectName(_fromUtf8("lineUsuario"))
        self.lblUsuario = QtGui.QLabel(self.groupBox)
        self.lblUsuario.setGeometry(QtCore.QRect(20, 50, 138, 51))
        self.lblUsuario.setStyleSheet(_fromUtf8("font: 75 10pt \"Verdana\";"))
        self.lblUsuario.setObjectName(_fromUtf8("lblUsuario"))
        self.btnValidar = QtGui.QPushButton(self.groupBox)
        self.btnValidar.setGeometry(QtCore.QRect(310, 190, 93, 28))
        self.btnValidar.setStyleSheet(_fromUtf8("font: 9pt \"Verdana\";\n"
"color: rgb(0, 170, 0);"))
        self.btnValidar.setObjectName(_fromUtf8("btnValidar"))
        self.lineEdit_2 = QtGui.QLineEdit(self.groupBox)
        self.lineEdit_2.setGeometry(QtCore.QRect(170, 110, 189, 22))
        self.lineEdit_2.setObjectName(_fromUtf8("lineEdit_2"))
        self.lblPass = QtGui.QLabel(self.groupBox)
        self.lblPass.setGeometry(QtCore.QRect(20, 90, 138, 51))
        self.lblPass.setStyleSheet(_fromUtf8("font: 75 10pt \"Verdana\";"))
        self.lblPass.setObjectName(_fromUtf8("lblPass"))
        self.lblMensaje = QtGui.QLabel(Dialog)
        self.lblMensaje.setGeometry(QtCore.QRect(80, 110, 421, 20))
        self.lblMensaje.setStyleSheet(_fromUtf8("font: 87 9pt \"Segoe UI Black\";\n"
"color:rgb(255, 0, 0);"))
        self.lblMensaje.setText(_fromUtf8(""))
        self.lblMensaje.setObjectName(_fromUtf8("lblMensaje"))

        self.retranslateUi(Dialog)
        QtCore.QMetaObject.connectSlotsByName(Dialog)

    def retranslateUi(self, Dialog):
        Dialog.setWindowTitle(_translate("Dialog", "LDPPython", None))
        self.lbl1.setText(_translate("Dialog", "<html><head/><body><p><span style=\" font-size:16pt; font-weight:600; color:#0055ff;\">Bienvenido al cajero Politécnico</span></p></body></html>", None))
        self.groupBox.setTitle(_translate("Dialog", "Autentificación", None))
        self.lblUsuario.setText(_translate("Dialog", "<html><head/><body><p align=\"center\"><span style=\" color:#55007f;\">Usuario</span></p></body></html>", None))
        self.btnValidar.setText(_translate("Dialog", "Siguiente", None))
        self.lblPass.setText(_translate("Dialog", "<html><head/><body><p align=\"center\"><span style=\" color:#55007f;\">Contraseña</span></p></body></html>", None))


try:
    _fromUtf8 = QtCore.QString.fromUtf8
except AttributeError:
    def _fromUtf8(s):
        return s

try:
    _encoding = QtGui.QApplication.UnicodeUTF8
    def _translate(context, text, disambig):
        return QtGui.QApplication.translate(context, text, disambig, _encoding)
except AttributeError:
    def _translate(context, text, disambig):
        return QtGui.QApplication.translate(context, text, disambig)

class Ui_ventanaPassword(object):
    def setupUi(self, ventanaPassword):
        ventanaPassword.setObjectName(_fromUtf8("ventanaPassword"))
        ventanaPassword.resize(616, 519)
        self.verticalLayoutWidget_5 = QtGui.QWidget(ventanaPassword)
        self.verticalLayoutWidget_5.setGeometry(QtCore.QRect(0, 20, 611, 488))
        self.verticalLayoutWidget_5.setObjectName(_fromUtf8("verticalLayoutWidget_5"))
        self.verticalLayout_6 = QtGui.QVBoxLayout(self.verticalLayoutWidget_5)
        self.verticalLayout_6.setMargin(0)
        self.verticalLayout_6.setObjectName(_fromUtf8("verticalLayout_6"))
        self.horizontalLayout = QtGui.QHBoxLayout()
        self.horizontalLayout.setObjectName(_fromUtf8("horizontalLayout"))
        self.verticalLayout = QtGui.QVBoxLayout()
        self.verticalLayout.setObjectName(_fromUtf8("verticalLayout"))
        spacerItem = QtGui.QSpacerItem(20, 40, QtGui.QSizePolicy.Minimum, QtGui.QSizePolicy.Expanding)
        self.verticalLayout.addItem(spacerItem)
        self.btn1 = QtGui.QPushButton(self.verticalLayoutWidget_5)
        self.btn1.setObjectName(_fromUtf8("btn1"))
        self.verticalLayout.addWidget(self.btn1)
        spacerItem1 = QtGui.QSpacerItem(20, 40, QtGui.QSizePolicy.Minimum, QtGui.QSizePolicy.Expanding)
        self.verticalLayout.addItem(spacerItem1)
        self.btn2 = QtGui.QPushButton(self.verticalLayoutWidget_5)
        self.btn2.setObjectName(_fromUtf8("btn2"))
        self.verticalLayout.addWidget(self.btn2)
        spacerItem2 = QtGui.QSpacerItem(20, 40, QtGui.QSizePolicy.Minimum, QtGui.QSizePolicy.Expanding)
        self.verticalLayout.addItem(spacerItem2)
        self.btn3 = QtGui.QPushButton(self.verticalLayoutWidget_5)
        self.btn3.setObjectName(_fromUtf8("btn3"))
        self.verticalLayout.addWidget(self.btn3)
        spacerItem3 = QtGui.QSpacerItem(20, 40, QtGui.QSizePolicy.Minimum, QtGui.QSizePolicy.Expanding)
        self.verticalLayout.addItem(spacerItem3)
        self.horizontalLayout.addLayout(self.verticalLayout)
        spacerItem4 = QtGui.QSpacerItem(40, 20, QtGui.QSizePolicy.Maximum, QtGui.QSizePolicy.Minimum)
        self.horizontalLayout.addItem(spacerItem4)
        self.verticalLayout_3 = QtGui.QVBoxLayout()
        self.verticalLayout_3.setObjectName(_fromUtf8("verticalLayout_3"))
        spacerItem5 = QtGui.QSpacerItem(20, 40, QtGui.QSizePolicy.Minimum, QtGui.QSizePolicy.Expanding)
        self.verticalLayout_3.addItem(spacerItem5)
        self.lbl1 = QtGui.QLabel(self.verticalLayoutWidget_5)
        self.lbl1.setStyleSheet(_fromUtf8("font: 75 8pt \"System\";\n"
"color:rgb(0, 170, 127);\n"
""))
        self.lbl1.setObjectName(_fromUtf8("lbl1"))
        self.verticalLayout_3.addWidget(self.lbl1)
        spacerItem6 = QtGui.QSpacerItem(20, 40, QtGui.QSizePolicy.Minimum, QtGui.QSizePolicy.Expanding)
        self.verticalLayout_3.addItem(spacerItem6)
        self.lbl2 = QtGui.QLabel(self.verticalLayoutWidget_5)
        self.lbl2.setStyleSheet(_fromUtf8("font: 75 8pt \"System\";\n"
"color:rgb(0, 170, 127);\n"
""))
        self.lbl2.setObjectName(_fromUtf8("lbl2"))
        self.verticalLayout_3.addWidget(self.lbl2)
        spacerItem7 = QtGui.QSpacerItem(20, 40, QtGui.QSizePolicy.Minimum, QtGui.QSizePolicy.Expanding)
        self.verticalLayout_3.addItem(spacerItem7)
        self.lbl3 = QtGui.QLabel(self.verticalLayoutWidget_5)
        self.lbl3.setStyleSheet(_fromUtf8("font: 75 8pt \"System\";\n"
"color:rgb(0, 170, 127);\n"
""))
        self.lbl3.setObjectName(_fromUtf8("lbl3"))
        self.verticalLayout_3.addWidget(self.lbl3)
        spacerItem8 = QtGui.QSpacerItem(20, 40, QtGui.QSizePolicy.Minimum, QtGui.QSizePolicy.Expanding)
        self.verticalLayout_3.addItem(spacerItem8)
        self.horizontalLayout.addLayout(self.verticalLayout_3)
        spacerItem9 = QtGui.QSpacerItem(40, 20, QtGui.QSizePolicy.Minimum, QtGui.QSizePolicy.Minimum)
        self.horizontalLayout.addItem(spacerItem9)
        self.verticalLayout_8 = QtGui.QVBoxLayout()
        self.verticalLayout_8.setObjectName(_fromUtf8("verticalLayout_8"))
        spacerItem10 = QtGui.QSpacerItem(20, 40, QtGui.QSizePolicy.Minimum, QtGui.QSizePolicy.Expanding)
        self.verticalLayout_8.addItem(spacerItem10)
        self.lbl4 = QtGui.QLabel(self.verticalLayoutWidget_5)
        self.lbl4.setLayoutDirection(QtCore.Qt.LeftToRight)
        self.lbl4.setStyleSheet(_fromUtf8("font: 75 8pt \"System\";\n"
"color:rgb(0, 170, 127);\n"
""))
        self.lbl4.setAlignment(QtCore.Qt.AlignRight|QtCore.Qt.AlignTrailing|QtCore.Qt.AlignVCenter)
        self.lbl4.setObjectName(_fromUtf8("lbl4"))
        self.verticalLayout_8.addWidget(self.lbl4)
        spacerItem11 = QtGui.QSpacerItem(20, 40, QtGui.QSizePolicy.Minimum, QtGui.QSizePolicy.Expanding)
        self.verticalLayout_8.addItem(spacerItem11)
        self.lbl5 = QtGui.QLabel(self.verticalLayoutWidget_5)
        self.lbl5.setStyleSheet(_fromUtf8("font: 75 8pt \"System\";\n"
"color:rgb(0, 170, 127);\n"
""))
        self.lbl5.setAlignment(QtCore.Qt.AlignRight|QtCore.Qt.AlignTrailing|QtCore.Qt.AlignVCenter)
        self.lbl5.setObjectName(_fromUtf8("lbl5"))
        self.verticalLayout_8.addWidget(self.lbl5)
        spacerItem12 = QtGui.QSpacerItem(20, 40, QtGui.QSizePolicy.Minimum, QtGui.QSizePolicy.Expanding)
        self.verticalLayout_8.addItem(spacerItem12)
        self.lbl6 = QtGui.QLabel(self.verticalLayoutWidget_5)
        self.lbl6.setStyleSheet(_fromUtf8("font: 75 8pt \"System\";\n"
"color:rgb(0, 170, 127);\n"
""))
        self.lbl6.setAlignment(QtCore.Qt.AlignRight|QtCore.Qt.AlignTrailing|QtCore.Qt.AlignVCenter)
        self.lbl6.setObjectName(_fromUtf8("lbl6"))
        self.verticalLayout_8.addWidget(self.lbl6)
        spacerItem13 = QtGui.QSpacerItem(20, 40, QtGui.QSizePolicy.Minimum, QtGui.QSizePolicy.Expanding)
        self.verticalLayout_8.addItem(spacerItem13)
        self.horizontalLayout.addLayout(self.verticalLayout_8)
        spacerItem14 = QtGui.QSpacerItem(40, 20, QtGui.QSizePolicy.Maximum, QtGui.QSizePolicy.Minimum)
        self.horizontalLayout.addItem(spacerItem14)
        self.verticalLayout_2 = QtGui.QVBoxLayout()
        self.verticalLayout_2.setObjectName(_fromUtf8("verticalLayout_2"))
        spacerItem15 = QtGui.QSpacerItem(20, 40, QtGui.QSizePolicy.Minimum, QtGui.QSizePolicy.Expanding)
        self.verticalLayout_2.addItem(spacerItem15)
        self.btn4 = QtGui.QPushButton(self.verticalLayoutWidget_5)
        self.btn4.setObjectName(_fromUtf8("btn4"))
        self.verticalLayout_2.addWidget(self.btn4)
        spacerItem16 = QtGui.QSpacerItem(20, 40, QtGui.QSizePolicy.Minimum, QtGui.QSizePolicy.Expanding)
        self.verticalLayout_2.addItem(spacerItem16)
        self.btn5 = QtGui.QPushButton(self.verticalLayoutWidget_5)
        self.btn5.setObjectName(_fromUtf8("btn5"))
        self.verticalLayout_2.addWidget(self.btn5)
        spacerItem17 = QtGui.QSpacerItem(20, 40, QtGui.QSizePolicy.Minimum, QtGui.QSizePolicy.Expanding)
        self.verticalLayout_2.addItem(spacerItem17)
        self.btn6 = QtGui.QPushButton(self.verticalLayoutWidget_5)
        self.btn6.setObjectName(_fromUtf8("btn6"))
        self.verticalLayout_2.addWidget(self.btn6)
        spacerItem18 = QtGui.QSpacerItem(20, 40, QtGui.QSizePolicy.Minimum, QtGui.QSizePolicy.Expanding)
        self.verticalLayout_2.addItem(spacerItem18)
        self.horizontalLayout.addLayout(self.verticalLayout_2)
        self.verticalLayout_6.addLayout(self.horizontalLayout)

        self.retranslateUi(ventanaPassword)
        QtCore.QMetaObject.connectSlotsByName(ventanaPassword)

    def retranslateUi(self, ventanaPassword):
        ventanaPassword.setWindowTitle(_translate("ventanaPassword", "Password", None))
        self.btn1.setText(_translate("ventanaPassword", "boton 1", None))
        self.btn2.setText(_translate("ventanaPassword", "boton 2", None))
        self.btn3.setText(_translate("ventanaPassword", "boton 3", None))
        self.lbl1.setText(_translate("ventanaPassword", "<html><head/><body><p>cambiar</p></body></html>", None))
        self.lbl2.setText(_translate("ventanaPassword", "<html><head/><body><p>cambiar</p></body></html>", None))
        self.lbl3.setText(_translate("ventanaPassword", "<html><head/><body><p>cambiar</p></body></html>", None))
        self.lbl4.setText(_translate("ventanaPassword", "<html><head/><body><p>cambiar</p></body></html>", None))
        self.lbl5.setText(_translate("ventanaPassword", "<html><head/><body><p>cambiar</p></body></html>", None))
        self.lbl6.setText(_translate("ventanaPassword", "<html><head/><body><p>cambiar</p></body></html>", None))
        self.btn4.setText(_translate("ventanaPassword", "boton 4", None))
        self.btn5.setText(_translate("ventanaPassword", "boton 5", None))
        self.btn6.setText(_translate("ventanaPassword", "boton 6", None))

#import recursos_rc
