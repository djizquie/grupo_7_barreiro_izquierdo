//
//  WindowNewUser.h
//  Program1
//
//  Created by Jhon Barreiro on 14/12/14.
//  Copyright (c) 2014 jhon barreiro. All rights reserved.
//

#import <Cocoa/Cocoa.h>

@interface WindowNewUser : NSWindowController
@property (weak) IBOutlet NSTextField *labelname;
@property (weak) IBOutlet NSView *labelcedula;
@property (weak) IBOutlet NSTextField *inputName;
@property (weak) IBOutlet NSTextField *inputCedula;
@property (weak) IBOutlet NSButton *buttonAceppt;

- (IBAction)buttonAceppt_clicked:(id)sender;

@end
