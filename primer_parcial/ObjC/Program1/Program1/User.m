//
//  User.m
//  Program1
//
//  Created by Jhon Barreiro on 14/12/14.
//  Copyright (c) 2014 jhon barreiro. All rights reserved.
//

#import "User.h"
#import <stdio.h>

@interface User ()


@end

@implementation User

NSString *content;

- (User*) initWithName:(NSString *)nombre{
    self = [super init];
    if (self){
        [self setNombre:nombre];
        secuenciaUser = nil;
    }
    return self;
}

- (User*) initWithName:(NSString *)nombre yCedula:(NSString *)cedula{
    self = [super init];
    if (self){
        nombreUser = nombre;
        cedulaUser = cedula;
    }
    return self;
}

- (BOOL) searchUser: (NSString*) nombre{
    
    NSURL *url = [NSURL fileURLWithPath:@"/Users/jhonbarreiro/Desktop/Usuarios.txt"];
    
    content = [NSString stringWithContentsOfURL:url encoding:NSUTF8StringEncoding error:nil];
    
    NSRange searchResult = [content rangeOfString:nombre];
    
    if (searchResult.location == NSNotFound) {
        return false;
    } else {
        return true;
    }
}

- (void) setNombre:(NSString *)nombre{
    nombreUser = nombre;
}

- (void) setCuenta: (int) cuenta{
    cuentaUser = cuenta;
}

- (void) setMonto: (float) monto{
    montoUser = monto;
}
- (float) getMonto{
    return montoUser;
}

- (int) getCuenta{
    return cuentaUser;
}

- (void) setClave: (int) clave{
    claveUser = clave;
}


- (void) setCedula:(NSString *)cedula{
    cedulaUser = cedula;
}

- (NSString*) getNombre{
    return nombreUser;
}

- (int) getClave{
    return claveUser;
}

- (NSString*) getSecuencia{
    return secuenciaUser;
}

- (void) asignarSecuencia:(int)claveNumerica{
    
    int digito = 0;
    NSMutableString *secTemp = [[NSMutableString alloc] init];
    
    for (int i = 0; i<4 ; i++ ){
    
        digito = claveNumerica % 10;
        claveNumerica /= 10;
        
        switch (digito) {
            case 0:
                [secTemp appendString:@"Ze"];
                break;
                
            case 1:
                [secTemp appendString:@"No"];
                break;
            
            case 2:
                [secTemp appendString:@"Do"];
                break;
                
            case 3:
                [secTemp appendString:@"Re"];
                break;
                
            case 4:
                [secTemp appendString:@"To"];
                break;
                
            case 5:
                [secTemp appendString:@"Ci"];
                break;
                
            case 6:
                [secTemp appendString:@"Si"];
                break;
                
            case 7:
                [secTemp appendString:@"Te"];
                break;
                
            case 8:
                [secTemp appendString:@"Ho"];
                break;
                
            case 9:
                [secTemp appendString:@"Ve"];
                break;
                
            default:
                break;
        }
        
    }
    secuenciaUser = secTemp;
    
    NSLog(@"Imprimiendo secTemp = %@", secTemp);
    NSLog(@"Imprimiendo secuenciaUser = %@", secuenciaUser);
}

- (NSString*) getCedula{
    return cedulaUser;
}

- (void) generarClave{
    claveUser = (arc4random() % 9000) + 1000;
}



@end
