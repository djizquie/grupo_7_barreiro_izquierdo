//
//  WindowNewUser.m
//  Program1
//
//  Created by Jhon Barreiro on 14/12/14.
//  Copyright (c) 2014 jhon barreiro. All rights reserved.
//

#import "WindowNewUser.h"

@interface WindowNewUser ()

@end

@implementation WindowNewUser

- (void)windowDidLoad {
    [super windowDidLoad];
    
    // Implement this method to handle any initialization after your window controller's window has been loaded from its nib file.
}

- (IBAction)buttonAceppt_clicked:(id)sender {
  
    [self close];
   
}
@end
