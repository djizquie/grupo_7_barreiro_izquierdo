//
//  AppDelegate.h
//  Program1
//
//  Created by Jhon Barreiro on 14/12/14.
//  Copyright (c) 2014 jhon barreiro. All rights reserved.
//

#import <Cocoa/Cocoa.h>
#import "WindowPassword.h"
#import "WindowNewUser.h"

@interface AppDelegate : NSObject <NSApplicationDelegate>

@property (nonatomic, retain) IBOutlet NSButton *button_LogIn;
@property (weak) IBOutlet NSTextField *inputText;
@property (weak) IBOutlet NSTextField *llblid;
@property (weak) IBOutlet NSTextField *lblerror;
@property  WindowPassword * windowPassword;
@property WindowNewUser * windowNewUser;
@property (weak) IBOutlet NSTextField *lblWelcome;
@property (weak) IBOutlet NSButton *buttonNewUser;


- (IBAction)logInButton:(id)sender;
- (IBAction)buttonNewUserClicked:(id)sender;

- (BOOL) estaRegistradoUsuario: (NSString*) nameUser;


@end

