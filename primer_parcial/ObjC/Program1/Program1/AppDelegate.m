//
//  AppDelegate.m
//  Program1
//
//  Created by Jhon Barreiro on 14/12/14.
//  Copyright (c) 2014 jhon barreiro. All rights reserved.
//

#import "AppDelegate.h"
#import "User.h"

@interface AppDelegate ()

@property (weak) IBOutlet NSWindow *window;
@end

@implementation AppDelegate

- (void)applicationDidFinishLaunching:(NSNotification *)aNotification {
    // Insert code here to initialize your application
}

- (void)applicationWillTerminate:(NSNotification *)aNotification {
    // Insert code here to tear down your application
}

- (BOOL) estaRegistradoUsuario:(NSString *)nameUser{
    

    return true;
}



- (IBAction)logInButton:(id)sender {
    
    NSString *inputUser, *sec = nil;
    User *user;

    
    
    inputUser = _inputText.stringValue; //Obtengo el string contenido en el TextField ID
    
    NSLog(@"%@",inputUser); //Imprimo el contenido (opcional)
    
    //--------------------------------------------------------
    
    NSURL *url = [NSURL fileURLWithPath:@"/Users/jhonbarreiro/Desktop/Usuarios.txt"]; //TENER EN CUENTA LA RUTA DEL ARCHIVO
    
    NSString *content = [NSString stringWithContentsOfURL:url encoding:NSUTF8StringEncoding error:nil];
    
    
    
    NSRange searchResult = [content rangeOfString:inputUser]; //Verifico que el nombre de usuario sea el correcto
    if (searchResult.location == NSNotFound) {
        NSLog(@"Usuario no encontrado");
        [self.lblerror setStringValue: @"Usuario no encontrado"];
    } else {
        
        user = [[User alloc]initWithName:inputUser];
        
        //NSString *models = @"Porsche,Ferrari,Maserati";
        NSArray *modelsAsArray = [content componentsSeparatedByString:@" "];
        
        [user setCedula:[modelsAsArray objectAtIndex:1]];
        NSLog(@"%@", [modelsAsArray objectAtIndex:1]);
        [user setClave:[[modelsAsArray objectAtIndex:2] intValue]];
        NSLog(@"%@", [modelsAsArray objectAtIndex:2]);
        [user setCuenta:[[modelsAsArray objectAtIndex:3] intValue]];
        NSLog(@"%@", [modelsAsArray objectAtIndex:3]);
        [user setMonto:[[modelsAsArray objectAtIndex:4] floatValue]];
        NSLog(@"Monto: %@", [modelsAsArray objectAtIndex:4]);
        NSLog(@"Clave: %i", [user getClave]);
        
        [user asignarSecuencia:[user getClave]];
        NSLog(@"[user getSecuencia] = : %@", [user getSecuencia]);
        sec = [user getSecuencia];
        NSLog(@"Imprimir sec: %@ ",sec);
        
        if (!_windowPassword){
            _windowPassword = [[WindowPassword alloc]initWithWindowNibName:@"WindowPassword"];
        }
        [_windowPassword showWindow:self]; //Muestro ventana donde se elegira contraseña
        [_windowPassword showSecs:sec]; // Preparo la ventana Password (lleno los label's)
        
       
        
        [self.window close]; //Cierro la ventana actual

        
    }
    
     //--------------------------------------------------------
    
    
    
    }

- (IBAction)buttonNewUserClicked:(id)sender {
    
    if (!_windowNewUser){
        _windowNewUser = [[WindowNewUser alloc]initWithWindowNibName:@"WindowNewUser"];
    }
    [_windowNewUser showWindow:self];
    
}
@end
