//
//  User.h
//  Program1
//
//  Created by Jhon Barreiro on 14/12/14.
//  Copyright (c) 2014 jhon barreiro. All rights reserved.
//

#import <Cocoa/Cocoa.h>
#import <Foundation/NSObject.h>

@interface User : NSObject{
    
    NSString *nombreUser;
    NSString *cedulaUser;
    NSString *secuenciaUser;
    int claveUser;
    float montoUser;
    int cuentaUser;


}

- (User*) initWithName: (NSString*) nombre;
- (User*) initWithName: (NSString*) nombre yCedula: (NSString*) cedula;
- (NSString*) getNombre;
- (NSString*) getCedula;
- (int) getClave;
- (int) getCuenta;
- (NSString*) getSecuencia;
- (void) generarClave;
- (void) setNombre: (NSString*) nombre;
- (void) setCedula: (NSString*) cedula;
- (void) setClave: (int) clave;
- (void) setCuenta: (int) cuenta;
- (void) setMonto: (float) monto;
- (float) getMonto;
- (void) asignarSecuencia: (int) claveNumerica;
- (BOOL) searchUser: (NSString*) nombre;


@end
