//
//  WindowPassword.m
//  Program1
//
//  Created by Jhon Barreiro on 14/12/14.
//  Copyright (c) 2014 jhon barreiro. All rights reserved.
//

#import "WindowPassword.h"

@interface WindowPassword ()<NSTextFieldDelegate>



@end


@implementation WindowPassword

- (void)windowDidLoad {
    [super windowDidLoad];
    cont = 0;
    NSLog(@"Se abrió window password");

}


- (void) showSecs: (NSString*) secuencia{
    
    NSString *consonants = @"BCDFGHJKLMNPQRTVWXYZ";
    NSString *vocals = @"aeiou";
    
    NSMutableArray *contentlabel;
    contentlabel = [NSMutableArray arrayWithObjects:@"",@"",@"",@"",@"",@"", nil];
    
    
    NSLog(@"Imprimiendo secuencia %@ desde showSecs en VentanaPassword",secuencia);
    
    int incr = 0, indice= 0;
    int al=0, al1=0, al2=0;
    NSMutableString * stringMutable = [[NSMutableString alloc]init];
    
    
    for (int i = 0; i < 6 ; i++){
        
        NSRange range = NSMakeRange(incr, 2);
        NSLog(@"%@", [secuencia substringWithRange:range]);
        incr += 2;
        if (i>=3)
            incr-=3;
        
        
        NSLog(@"Bien 1");
        
        al = arc4random()%6; //Numero aleatorio entre 0 - 5
        NSLog(@"valor de al: %i", al);
        //al++;
        
        NSLog(@"Par de letras que se van a insertar: %@ ", [secuencia substringWithRange:range]);
        
        [stringMutable appendFormat:@"%@", [secuencia substringWithRange:range]];
        [stringMutable appendString: @"-"];
        
        
        NSLog(@"Bien 2");
        
        for (int j = 0; j < 4 ; j++){
            
            al1 = arc4random()%[consonants length];
            al2 = arc4random()%[vocals length];
            
            NSLog(@"valor de al1: %i",al1);
            NSLog(@"valor de al2: %i",al2);
            
            NSRange range2 = NSMakeRange(al1, 1);
            NSRange range3 = NSMakeRange(al2, 1);
            
            
            [stringMutable appendFormat:@"%@", [consonants substringWithRange:range2]];//Se añade consonante
            NSLog(@"%@", stringMutable);
            NSLog(@"Bien 3");
            
            [stringMutable appendFormat:@"%@", [vocals substringWithRange:range3]]; //Se añade vocal
            NSLog(@"%@", stringMutable);
        
            NSLog(@"Bien 4");
        
            if (j != 3){
                [stringMutable appendString: @"-"];
            }
            else{
                NSLog(@"Indice en el que se añadirá: %i",indice);
                NSLog(@"(i= %i,j= %i)",i,j);
                [contentlabel insertObject:stringMutable atIndex:indice];//añade el string en la posición indice
                stringMutable = [[NSMutableString alloc]init];
                indice++;
                NSLog(@"Bien 5");
            }
        }
    }
    
    for (int k = 0; k < 6 ; k++){
        switch (k) {
            case 0:
                [self.labelSec1 setStringValue: [contentlabel objectAtIndex:k]];
                NSLog(@"Secuencia en indice %i: %@",k,[contentlabel objectAtIndex:k]);
                break;
            case 1:
                [self.labelSec2 setStringValue: [contentlabel objectAtIndex:k]];
                NSLog(@"Secuencia en indice %i: %@",k,[contentlabel objectAtIndex:k]);
                break;
            case 2:
                [self.labelSec3 setStringValue: [contentlabel objectAtIndex:k]];
                NSLog(@"Secuencia en indice %i: %@",k,[contentlabel objectAtIndex:k]);
                break;
            case 3:
                [self.labelSec4 setStringValue: [contentlabel objectAtIndex:k]];
                NSLog(@"Secuencia en indice %i: %@",k,[contentlabel objectAtIndex:k]);
                break;
            case 4:
                [self.labelSec5 setStringValue: [contentlabel objectAtIndex:k]];
                NSLog(@"Secuencia en indice %i: %@",k,[contentlabel objectAtIndex:k]);
                break;
            case 5:
                [self.labelSec6 setStringValue: [contentlabel objectAtIndex:k]];
                NSLog(@"Secuencia en indice %i: %@",k,[contentlabel objectAtIndex:k]);
                break;
                
            default:
                break;
        }
        
    }
    
}

- (IBAction)buttonIngresar_clicked:(id)sender {
    if (cont == 4){
        NSLog(@"Ingreso satisfactorio"); //Aquí debería salir una ventana emergente pero no reconoce la clase UIAlertView :(
    }
    else {
        [self.labelErrorPassword setStringValue:@"Contraseña Incorrecta"];
        cont = 0;
    }
        
    
}

- (IBAction)botton1clicked:(id)sender {
    
    NSLog(@"%@",[self.labelSec1 stringValue]);
    NSRange detectar;
    
    if (detectar.location == NSNotFound) {
               NSLog(@"La palabra que buscas no existe.");
    }
    else{
        cont ++;
    }
}

- (IBAction)botton2clicked:(id)sender {
    NSLog(@"%@",[self.labelSec2 stringValue]);
    NSRange detectar;
    
    if (detectar.location == NSNotFound) {
        NSLog(@"La palabra que buscas no existe.");
    }
    else{
        cont ++;
    }
}

- (IBAction)botton3clicked:(id)sender {
    NSLog(@"%@",[self.labelSec3 stringValue]);
    NSRange detectar;
    
    if (detectar.location == NSNotFound) {
        NSLog(@"La palabra que buscas no existe.");
    }
    else{
        cont ++;
    }
}

- (IBAction)botton4clicked:(id)sender {
    NSLog(@"%@",[self.labelSec4 stringValue]);
    NSRange detectar;
    
    if (detectar.location == NSNotFound) {
        NSLog(@"La palabra que buscas no existe.");
    }
    else{
        cont ++;
    }
}

- (IBAction)botton5clicked:(id)sender {
    NSLog(@"%@",[self.labelSec5 stringValue]);
    NSRange detectar;
    
    if (detectar.location == NSNotFound) {
        NSLog(@"La palabra que buscas no existe.");
    }
    else{
        cont ++;
    }
}

- (IBAction)botton6clicked:(id)sender {
    NSLog(@"%@",[self.labelSec6 stringValue]);
    NSRange detectar;
    
    if (detectar.location == NSNotFound) {
        NSLog(@"La palabra que buscas no existe.");
    }
    else{
        cont ++;
    }
}


@end
