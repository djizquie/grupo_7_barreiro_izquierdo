//
//  main.m
//  Program1
//
//  Created by Jhon Barreiro on 14/12/14.
//  Copyright (c) 2014 jhon barreiro. All rights reserved.
//

#import <Cocoa/Cocoa.h>

int main(int argc, const char * argv[]) {
    return NSApplicationMain(argc, argv);
}
