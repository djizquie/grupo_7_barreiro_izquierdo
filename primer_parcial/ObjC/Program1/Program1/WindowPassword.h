//
//  WindowPassword.h
//  Program1
//
//  Created by Jhon Barreiro on 14/12/14.
//  Copyright (c) 2014 jhon barreiro. All rights reserved.
//

#import <Cocoa/Cocoa.h>

@interface WindowPassword : NSWindowController{

    //NSString *secuencia;
    int cont;
    
    
}
@property (weak) IBOutlet NSButton *button1;
@property (weak) IBOutlet NSButton *button2;
@property (weak) IBOutlet NSButton *button3;
@property (weak) IBOutlet NSButton *button4;
@property (weak) IBOutlet NSButton *button5;
@property (weak) IBOutlet NSButton *button6;
@property (weak) IBOutlet NSTextField *labelSec1;
@property (weak) IBOutlet NSTextField *labelSec2;
@property (weak) IBOutlet NSTextField *labelSec3;
@property (weak) IBOutlet NSTextField *labelSec4;
@property (weak) IBOutlet NSTextField *labelSec5;
@property (weak) IBOutlet NSTextField *labelSec6;
@property (weak) IBOutlet NSButton *botonIngresar;
@property (weak) IBOutlet NSTextField *labelErrorPassword;

- (void) showSecs: (NSString*) secuencia;
- (IBAction)buttonIngresar_clicked:(id)sender;
- (IBAction)botton1clicked:(id)sender;
- (IBAction)botton2clicked:(id)sender;
- (IBAction)botton3clicked:(id)sender;
- (IBAction)botton4clicked:(id)sender;
- (IBAction)botton5clicked:(id)sender;
- (IBAction)botton6clicked:(id)sender;





@end
